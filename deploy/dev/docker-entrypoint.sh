#!/bin/bash

echo "Collect static files"
python manage.py collectstatic --no-input

echo "Apply databases migrations"
python manage.py makemigrations
python manage.py migrate

echo "Population of database"
python manage.py clean
python manage.py populate

echo "Starting server"
python manage.py runserver 0.0.0.0:8000
