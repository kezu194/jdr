function compare_old_and_new_value(new_value, default_value, input, classname, display_points) {
    var inputs = document.getElementsByClassName(classname);
    check_min_values(inputs);

    let max = 40;
    if (document.getElementsByClassName("survivant")[0].checked === true) {
        max = 42;
    } else
    if (document.getElementsByClassName("active")[0].id === "pills-super-mutant-tab") {
        max = 44;
    }

    check_max_values(inputs, input, display_points, max, new_value);
    let remaining_points = document.getElementById("remaining_points");
    if (remaining_points.textContent === "0") {
        document.getElementById("step_3").className = "";
    }
    else {
        clear_step_3();
        document.getElementById("step_3").className = "d-none";
    }
}

function check_max_values(inputs, input, display_points, max, new_value) {
    var somme = get_somme_totale(inputs);
    let remaining_points = document.getElementById(display_points);
    var input_max = parseInt(input.max, 10);
    if (somme > max || input.value > input_max) {
        let new_value = 1;
        while (somme-new_value>max || input.value - new_value > input_max) {
            new_value += 1;
        }
        input.value -= new_value;
        somme = get_somme_totale(inputs);
    }
    remaining_points.text = max - somme;
}

function check_min_values(inputs) {
    for (let id=0; id<inputs.length; id++) {
        var input = inputs[id];
        var diff = 0;
        var min = parseInt(input.min, 10);
        if (input.value < min) {
            diff += min - input.value;
            input.value = min;
        }
        if (diff>0) {
            let remaining_points = document.getElementById("remaining_points");
            remaining_points.text = parseInt(remaining_points, 10) + diff;
        }
    }
}

function get_somme_totale(inputs) {
    var somme = 0;
    for (let input=0; input<inputs.length; input++) {
        somme += parseInt(inputs[input].value, 10);
    }
    return somme;
}

function clear_step_3() {
    var checkboxs = document.getElementsByClassName("atout");
    var inputs = document.getElementsByClassName("rang");
    for (let id=0; id<inputs.length; id++) {
        checkboxs[id].checked = false;
        checkboxs[id].disabled = false;
        inputs[id].value = 0;
    }

    document.getElementById("atout_compteur").textContent = "0";
    document.getElementById("competence_points").textContent = "14";
    document.getElementById("max_competence").textContent = "14";

    clear_step_4();
    hide_step_4();
}

function hide_step_3() {
    document.getElementById("step_3").className="d-none";
}
