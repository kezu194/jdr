const data = JSON.parse(document.getElementById("aptitudes").textContent);
var keys = Object.keys(data);
var values = Object.values(data);

function create_options() {
    let select = document.getElementById("select-aptitude");
    let second_select = document.getElementById("select-aptitude-2")
    for (let id=0; id<keys.length; id++) {
        let option_text = values[id].name
        let list_key = Object.keys(values[id].prerequis.SPECIAL);
        if (list_key.length > 0) {
            option_text += " (";
            for (let prerequis=0; prerequis<list_key.length; prerequis++) {
                let key = list_key[prerequis];
                option_text += list_key[prerequis] + ": " + values[id].prerequis.SPECIAL[key];
                if (prerequis < list_key.length - 1) {
                    option_text += ", ";
                }
            }
            option_text += ")"
        }

        let option_value = keys[id];
        var new_option = new Option(option_text, option_value);
        new_option.className = "option";
        new_option.addEventListener('click', display_aptitude);
        select.add(new_option);

        new_option = new Option(option_text, option_value);
        new_option.className = "option";
        new_option.addEventListener('click', display_aptitude_2);
        second_select.add(new_option);
    }
    display_options();
}

function display_step_5(event) {
    let select = document.getElementById("select-aptitude");
    let second_select = document.getElementById("select-aptitude-2");
    let step_5 = document.getElementById("step_5");
    let aptitude_2 = document.getElementById("aptitude_2");
    if (select.selectedIndex !== 0 && aptitude_2.className !== "") {
        step_5.className = "";
        activate_submit_button(false);
        return ;
    }
    if (select.selectedIndex !== 0 && aptitude_2.className === "" && second_select.selectedIndex !== 0) {
        step_5.className = "";
        activate_submit_button(false);
    }
    else {
        step_5.className = "d-none";
        activate_submit_button(true);
    }
}

function activate_submit_button(disabled) {
    let submit = document.getElementById("submit");
    submit.disabled = disabled;
    if (disabled === false) {
        submit.style.opacity = "1";
    }
    else {
        submit.style.opacity = "0.5";
    }

}

function check_if_available(prerequis, special) {
    if (prerequis.LVL[0] > 1) {
        return false;
    }
    let keys_prerequis = Object.keys(prerequis.SPECIAL)
    for (let attribut=0; attribut<keys_prerequis.length; attribut++) {
        if (keys_prerequis[attribut] === "FOR" && prerequis.SPECIAL.FOR > special.FOR) {
            return false;
        }
        if (keys_prerequis[attribut] === "PER" && prerequis.SPECIAL.PER > special.PER) {
            return false;
        }
        if (keys_prerequis[attribut] === "END" && prerequis.SPECIAL.END > special.END) {
            return false;
        }
        if (keys_prerequis[attribut] === "CHR" && prerequis.SPECIAL.CHR > special.CHR) {
            return false;
        }
        if (keys_prerequis[attribut] === "INT" && prerequis.SPECIAL.INT > special.INT) {
            return false;
        }
        if (keys_prerequis[attribut] === "AGI" && prerequis.SPECIAL.AGI > special.AGI) {
            return false;
        }
        if (keys_prerequis[attribut] === "CHA" && prerequis.SPECIAL.CHA > special.CHA) {
            return false;
        }
    }
    return true;
}

function display_options() {
    aptitudes = document.getElementsByClassName("option");
    const special = {
        FOR: document.getElementById("force").value,
        PER: document.getElementById("perception").value,
        END: document.getElementById("endurance").value,
        CHR: document.getElementById("charisme").value,
        INT: document.getElementById("intelligence").value,
        AGI: document.getElementById("agilite").value,
        CHA: document.getElementById("chance").value,
    };
    for (let id=0; id<values.length; id++) {
        if (check_if_available(values[id].prerequis, special) === true) {
            aptitudes[id].style.display = "block";
            aptitudes[id+values.length].style.display = "block";
        }
        else {
            aptitudes[id].style.display = "none";
            aptitudes[id+values.length].style.display = "none";
        }
    }
}

function display_aptitude(event){
    let aptitude = event.target;
    let title = document.getElementById("title");
    let rang = document.getElementById("rang-aptitude");
    let prerequis = document.getElementById("prerequis");
    let description = document.getElementById("description");

    title.textContent = data[aptitude.value].name;
    rang.textContent = "Rangs : " + data[aptitude.value].nb_rangs;
    prerequis.textContent = display_prerequis(data[aptitude.value].prerequis.SPECIAL);
    description.textContent = data[aptitude.value].description;
}

function display_aptitude_2(event){
    let aptitude = event.target;
    let title = document.getElementById("title-2");
    let rang = document.getElementById("rang-aptitude-2");
    let prerequis = document.getElementById("prerequis-2");
    let description = document.getElementById("description-2");

    title.textContent = data[aptitude.value].name;
    rang.textContent = "Rangs : " + data[aptitude.value].nb_rangs;
    prerequis.textContent = display_prerequis(data[aptitude.value].prerequis.SPECIAL);
    description.textContent = data[aptitude.value].description;
}

function display_prerequis(prerequis) {
    let keys = Object.keys(prerequis);
    if (keys.length === 0) {
        return "Prérequis : aucun";
    }
    var text = "Prérequis : ";
    for (let id=0; id<keys.length; id++) {
        if (id > 0) {
            text += ", "
        }
        text += keys[id] + " " + prerequis[keys[id]]
    }
    return text;
}

function clear_select_1() {
    var title = document.getElementById("title");
    var rang = document.getElementById("rang-aptitude");
    var prerequis = document.getElementById("prerequis");
    var description = document.getElementById("description");
    title.textContent = "";
    rang.textContent = "";
    prerequis.textContent = "";
    description.textContent = "";
}

function clear_select_2() {
    title = document.getElementById("title-2");
    rang = document.getElementById("rang-aptitude-2");
    prerequis = document.getElementById("prerequis-2");
    description = document.getElementById("description-2");
    title.textContent = "";
    rang.textContent = "";
    prerequis.textContent = "";
    description.textContent = "";
}

function clear_step_4() {

    clear_select_1();
    clear_select_2();

    var select = document.getElementById("select-aptitude");
    select.value = "default";
    select = document.getElementById("select-aptitude-2");
    select.value = "default";
    display_step_5()
}



let special = document.getElementsByClassName("special");
for (let id=0; id<special.length; id++) {
    special[id].addEventListener('change', display_options);
}