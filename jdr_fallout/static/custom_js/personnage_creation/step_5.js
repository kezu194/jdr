var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'))
var popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
  return new bootstrap.Popover(popoverTriggerEl)
})

function calculate_stats() {
    const special = {
        FOR: parseInt(document.getElementById("force").value, 10),
        PER: parseInt(document.getElementById("perception").value, 10),
        END: parseInt(document.getElementById("endurance").value, 10),
        CHR: parseInt(document.getElementById("charisme").value, 10),
        INT: parseInt(document.getElementById("intelligence").value, 10),
        AGI: parseInt(document.getElementById("agilite").value, 10),
        CHA: parseInt(document.getElementById("chance").value, 10),
    };
    let survivants = document.getElementsByClassName("survivant");
    let actives = document.getElementsByClassName("active");

    let pv = document.getElementById("pv");
    let defense = document.getElementById("defense");
    let initiative = document.getElementById("initiative");
    let charge = document.getElementById("charge_max");
    let melee = document.getElementById("melee");

    pv.textContent = special.END + special.CHA;
    if (special.AGI <= 8) {
        defense.textContent = 1
    }
    else {
        defense.textContent = 2
    }
    initiative.textContent = special.PER + special.AGI;

    if (survivants[2].checked === true) {
        charge.textContent = (75 + special.FOR * 2.5) + " KG";
    } else
    if (actives[0].id === "pills-mister-handy-tab") {
        charge.textContent = 75 + " KG";
    } else {
        charge.textContent = (75 + special.FOR * 5) + " KG";
    }

    var degat = 0;
    if (special.FOR >= 7 && special.FOR <= 8) {
        degat = 1;
    }
    if (special.FOR >= 9 && special.FOR <= 10) {
        degat = 2;
    }
    if (special.FOR >= 11) {
        degat = 3;
    }
    melee.textContent = degat;
    if (survivants[3].checked === true) {
        melee.textContent += " (+1)";
    }
}

for (let id=0; id<special.length; id++) {
    special[id].addEventListener('change', calculate_stats);
}