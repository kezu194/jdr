function limit_atout_option() {
    let total_checked = 0;
    let atouts = document.getElementsByClassName("atout");
    for (let checkbox_id=0; checkbox_id<atouts.length; checkbox_id++) {
        let element = atouts[checkbox_id]
        if (element.checked === true) {
            total_checked += 1;
        }
    }
    let max = document.getElementById("max_atout_number").textContent;
    if (total_checked === parseInt(max, 10)) {
        for (let element_id=0; element_id<atouts.length; element_id++) {
            if (atouts[element_id].checked === false) {
                atouts[element_id].disabled = true;
            }
        }
        document.getElementById("atout_compteur").textContent = max;
        return ;
    }

    let origine = document.getElementsByClassName("active")[0];
    if (origine.id === "pills-confrerie-tab") {
        let energie = document.getElementById("atout-0");
        let reparation = document.getElementById("atout-13");
        let science = document.getElementById("atout-14");
        if (total_checked === parseInt(max, 10) - 1 && energie.checked === false && science.checked === false && reparation.checked === false) {
            for (let element_id=0; element_id<atouts.length; element_id++) {
                if (atouts[element_id].checked === false && element_id !== 0 && element_id !== 13 && element_id !== 14) {
                    atouts[element_id].disabled = true;
                }
                else {
                    atouts[element_id].disabled = false;
                }
            }
            document.getElementById("atout_compteur").textContent = total_checked;
            return ;
        }
    }

    for (let id=0; id<atouts.length; id++) {
        let actives = document.getElementsByClassName("active")
        if ((document.getElementById("turn_to_goul").checked === true || actives[0].id === "pills-goule-tab") && id===15) {
            atouts[id].disabled = true;
        }
        else {
            atouts[id].disabled = false;
        }

    }
    document.getElementById("atout_compteur").textContent = total_checked;
}

function add_in_rang(event){
    let checkbox = event.target;
    let atout_id = checkbox.id.substring(6);
    rang = document.getElementById("rang-"+atout_id);

    let node_points = document.getElementById("competence_points");
    var points = parseInt(node_points.textContent, 10);
    if (checkbox.checked === true) {
        if (rang.value <= 2) {
            points += parseInt(rang.value, 10);
            node_points.textContent = points;
            rang.value = 2;
        }
        if (rang.value > 2) {
            node_points.textContent = points + 2;
        }
    }
    else {
        rang.value = parseInt(rang.value, 10) -2;
    }
}

function manage_competence_rangs(default_value, input, classname, display_points){
    check_min_input(input);
    check_max_input(input);
    set_max_and_actual_points();
    hide_or_display_step_4();
}

function check_max_input(input) {
    if (input.value > 3) {
        input.value = 3;
    }
    var somme = get_total_points()
    var max = set_max_points();
    while (somme > max) {
        input.value -= 1;
        somme -= 1;
    }
}

function check_min_input(input) {
    let atout_id = input.id.substring(5);
    atout = document.getElementById("atout-"+atout_id);
    var min = 0;
    if (atout.checked === true) {
        min = 2;
    }
    var diff = 0;
    if (input.value<min) {
        diff += min - input.value;
        input.value = min;
    }
    if (diff>0) {
        let remaining_points = document.getElementById("competence_points");
        remaining_points.text = parseInt(remaining_points, 10) + diff;
    }

}

function set_max_and_actual_points() {
    let max = parseInt(document.getElementById("intelligence").value, 10) + 9;
    let total_points = get_total_points();
    let remaining_points = document.getElementById("competence_points");
    remaining_points.textContent = max - total_points;
}

function set_max_points(){
    let max = parseInt(document.getElementById("intelligence").value, 10) + 9;
    document.getElementById("max_competence").textContent = max;
    document.getElementById("competence_points").textContent = max;
    return max;
}

function get_total_points() {
    let rangs = document.getElementsByClassName("rang");
    let atouts = document.getElementsByClassName("atout");
    var somme = 0;
    for (let id=0; id<rangs.length; id++) {
        atout = atouts[id];
        rang = rangs[id];
        somme += parseInt(rang.value, 10);
        if (atout.checked === true) {
            somme -= 2;
        }
    }
    return somme;
}

function hide_step_4() {
    document.getElementById("step_4").className = "d-none";
}

function display_step_4() {
    let step_4 = document.getElementById("step_4");
    let atout_compteur = document.getElementById("atout_compteur");
    let actual = atout_compteur.textContent;
    let max = document.getElementById("max_atout_number").textContent;
    if (actual === max) {
        step_4.className = "";
    }
}

function hide_or_display_step_4() {
    let atout_compteur = document.getElementById("atout_compteur");
    var min = atout_compteur.textContent;
    var max = document.getElementById("max_atout_number").textContent;
    let nb_points = document.getElementById("competence_points").textContent;

    if (min === max && nb_points === "0") {
        return display_step_4()
    }
    hide_step_4()
}


let node_atout = document.getElementsByClassName("atout");
for (checkbox_id=0; checkbox_id<node_atout.length; checkbox_id++){
    node_atout[checkbox_id].addEventListener('change', limit_atout_option);
    node_atout[checkbox_id].addEventListener('change', add_in_rang);
    node_atout[checkbox_id].addEventListener('change', hide_or_display_step_4);
}
document.getElementById("intelligence").addEventListener('change', set_max_points);
