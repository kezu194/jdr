function limit_survivant_option() {
    let total_checked = 0;
    let elements = document.getElementsByClassName("survivant");

    for (let checkbox_id=1; checkbox_id<=elements.length; checkbox_id++) {
        let element = document.getElementById("checkbox-"+checkbox_id);
        if (element.checked === true) {
            total_checked += 1;
        }
        if (total_checked === 2) {
            for (let element_id=0; element_id<elements.length; element_id++) {
                if (elements[element_id].checked === false) {
                    elements[element_id].disabled = true;
                }
            }
            document.getElementById("trait_compteur").textContent = "2/2";
            return false;
        }
    }

    for (element_id=0; element_id<elements.length; element_id++) {
        elements[element_id].disabled = false;
    }
    document.getElementById("trait_compteur").textContent = total_checked + "/2";
}

let elements = document.getElementsByClassName("survivant");
for (checkbox_id=0; checkbox_id<elements.length; checkbox_id++){
    elements[checkbox_id].addEventListener('change', limit_survivant_option);
    elements[checkbox_id].addEventListener('change', clear_step_1);
}

function turn_to_goul() {
    let checkbox = document.getElementById("turn_to_goul");
    let trait_goule = document.getElementById("trait-goule");
    let trait_abri = document.getElementById("trait-abri");
    if (checkbox.checked === true) {
        trait_goule.style.display = "block";
        trait_abri.style.display = "none";
    } else {
        trait_goule.style.display = "none";
        trait_abri.style.display = "block";
    }
    clear_step_1();
}

function goule_origine() {
    document.getElementById("max_atout_number").textContent = 4;
    document.getElementById("atout-15").checked = true;
    document.getElementById("atout-15").disabled = true;
    let rang = document.getElementById("rang-15");
    rang.value = 2;
    rang.min = 2;
    document.getElementById("atout_compteur").textContent = "1";
}

function clear_step_1() {
    clear_step_2();
    let actives = document.getElementsByClassName("active")
    if (actives.length > 0) {
        undo_origine(actives);
        var display_step_2 = true;

        document.getElementById("origine-choice").value = actives[0].outerText;
        if (actives[0].id === "pills-survivant-tab") {
            display_step_2 = manage_if_survivant_is_choose();
        }
        if (actives[0].id === "pills-habitant-tab") {
            document.getElementById("max_atout_number").textContent = 4;
            if (document.getElementById("turn_to_goul").checked === true) {
                goule_origine();
            }
        }
        if (actives[0].id === "pills-confrerie-tab") {
            document.getElementById("max_atout_number").textContent = 4;
        }
        if (actives[0].id === "pills-goule-tab") {
            goule_origine();
        }

        let specials = document.getElementsByClassName("special");
        if (actives[0].id === "pills-super-mutant-tab") {
            specials[0].min = 6;
            specials[0].max = 12;
            specials[0].value = 6;
            specials[2].min = 6;
            specials[2].max = 12;
            specials[2].value = 6;
            specials[3].max = 6;
            specials[4].max = 6;

            document.getElementById("remaining_points").textContent = 7;
        }

        if (display_step_2 === true) {
            document.getElementById("step_2").className = "";
        }
    }
}

function undo_origine(actives) {
    if (actives[0].id !== "pills-survivant-tab") {
        let survivants = document.getElementsByClassName("survivant");
        for (let checkbox=0; checkbox<survivants.length; checkbox++) {
            survivants[checkbox].checked = false;
        }
        document.getElementById("trait_compteur").textContent = "0/2";
    }
    if (actives[0].id !== "pills-habitant-tab") {
        document.getElementById("turn_to_goul").checked = false;
        let trait_goule = document.getElementById("trait-goule");
        let trait_abri = document.getElementById("trait-abri");
        trait_goule.style.display = "none";
        trait_abri.style.display = "block";
    }
    if (actives[0].id !== "pills-confrerie-tab") {
        document.getElementById("max_atout_number").textContent = 3;
    }
    if (actives[0].id !== "pills-goule-tab") {
        document.getElementById("max_atout_number").textContent = 3;
    }
    let specials = document.getElementsByClassName("special");
    if (actives[0].id !== "pills-super-mutant-tab") {
        specials[0].min = 4;
        specials[0].max = 10;
        specials[0].value = 5;
        specials[2].min = 4;
        specials[2].max = 10;
        specials[2].value = 5;
        specials[3].max = 10;
        specials[4].max = 10;
    }
}

function manage_if_survivant_is_choose() {
    let step_2 = document.getElementById("step_2");
    let checkboxs = document.getElementsByClassName("survivant");
    if (document.getElementById("trait_compteur").textContent !== "0/2") {
        step_2.className = "";
        let attributs = document.getElementsByClassName("special");

        if (document.getElementById("trait_compteur").textContent === "1/2"){
            document.getElementById("aptitude_2").className = "";
        }
        else {
            document.getElementById("aptitude_2").className = "d-none";
        }

        if (checkboxs[0].checked === true) {
            document.getElementById("remaining_points").textContent = 7;
        }

        if (checkboxs[1].checked === true) {
            document.getElementById("max_atout_number").textContent = "4";
        }
        else {
            document.getElementById("max_atout_number").textContent = "3";
        }
        return true;
    }
    else {
        step_2.className = "d-none";
        return false;
    }
}

function display_step_1() {
    let step_1 = document.getElementById("step_1")
    if (document.getElementById("nom_personnage").value !== "") {
        step_1.className = "";
    }
    else {
        step_1.className = "d-none";
        document.getElementsByClassName("active")[0].className = "nav-link origine";
        document.getElementsByClassName("active")[0].className = "tab-pane fade";
        clear_step_2();
    }
}

function clear_step_2() {
    document.getElementById("step_2").className = "d-none";
    var inputs = document.getElementsByClassName("special");
    for (let id=0; id<inputs.length; id++) {
        inputs[id].value = 5;
    }
    document.getElementById("remaining_points").text = 5;
    hide_step_3();
    clear_step_3();
}

let origines = document.getElementsByClassName("origine");
for (let id=0; id<origines.length; id++) {
    origines[id].addEventListener('click', clear_step_1)
}

document.getElementById("turn_to_goul").addEventListener('change', turn_to_goul);
window.addEventListener("load", (event) => {
    limit_survivant_option();
    turn_to_goul();
    clear_step_1();
    create_options(data);
    document.getElementById("nom_personnage").value = "";

});
