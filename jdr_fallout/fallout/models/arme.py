from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

from fallout.models.effet_arme import EffetArme
from fallout.models.qualites import Qualites
from fallout.models.munition import ModelMunition, Munition
from fallout.models.item import Item
from fallout.models.personnage import Personnage


class TypeArme(models.TextChoices):
    """
    Enum décrivant les différentes classes(types) d'Arme possible au choix
    """
    LOURDE = "Lourde"
    ENERGETIQUE = "Énergétique"
    EXPLOSIF = "Explosif"
    CORPSACORPS = "Corps à corps"
    LEGERE = "Légère"
    PROJECTILE = "Projectile"
    MAINSNUES = "Mains nues"


class TypeDegat(models.TextChoices):
    BALISTIQUE = "Balistique"
    ENERGETIQUE = "Énergétique"
    RADIATION = "Radiation"
    POISON = "Poison"


class Portee(models.TextChoices):
    COURTE = "Courte"
    MOYENNE = "Moyenne"
    LONGUE = "Longue"
    EXTREME = "Extrême"


class ModelArme(Item):
    """
    Class décrivant une arme.
    """
    type = models.CharField(max_length=100, choices=TypeArme.choices)
    degat = models.IntegerField(validators=[MinValueValidator(1)])
    effet = models.ManyToManyField(EffetArme, blank=True)
    type_degat = models.CharField(max_length=100, null=True,
                                  choices=TypeDegat.choices)
    qualite = models.ManyToManyField(Qualites, blank=True)
    perforation = models.IntegerField(null=True,
                                      validators=[MinValueValidator(0)])
    type_degat_persistance = models.CharField(max_length=100, null=True,
                                              choices=TypeDegat.choices)
    rarete = models.IntegerField(validators=[MinValueValidator(1)])
    bonus_furtivite = models.IntegerField(default=0)

    def __str__(self):
        return self.name


class ArmeLootable(ModelArme):
    """
    Rassemble toutes les armes lootables, distante et de mêlée.
    """
    def __str__(self):
        return super().__str__()


class ArmeDistanteLootable(ArmeLootable):
    """
    Rassemble toutes les armes distantes lootables pour les joueurs.
    """
    cadence = models.IntegerField(null=True, validators=[MinValueValidator(0),
                                                         MaxValueValidator(6)])
    portee = models.CharField(max_length=100, choices=Portee.choices,
                              null=True)
    munition_type = models.ForeignKey(ModelMunition, on_delete=models.CASCADE,
                                      null=True)
    # Exclusif au mousquet laser
    nombre_munition_par_tir = models.IntegerField(default=1)

    def __str__(self):
        return super().__str__()


class Arme(ModelArme):
    """
    Arme détenue par un joueur.
    """
    based_on = models.ForeignKey(ArmeLootable, on_delete=models.CASCADE,
                                 null=True, blank=True)
    proprietaire = models.ForeignKey(Personnage, on_delete=models.CASCADE)

    def __str__(self):
        return super().__str__()


class ArmeDistante(Arme):
    """
    Arme distante détenue par un joueur
    """
    cadence = models.IntegerField(null=True, validators=[MinValueValidator(0),
                                                         MaxValueValidator(6)])
    portee = models.CharField(max_length=100, choices=Portee.choices,
                              null=True)
    munition_type = models.ForeignKey(Munition, on_delete=models.CASCADE,
                                      null=True)
    
    # Exclusif au mousquet laser
    nombre_munition_par_tir = models.IntegerField(default=1)

    def __str__(self):
        return super().__str__()
