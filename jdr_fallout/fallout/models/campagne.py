from django.db import models


class Campagne(models.Model):
    """
    Class décrivant une campagne effectuée par des Personnages.
    """
    name = models.CharField(max_length=32)
    xp_point = models.IntegerField()

    # desc = models.CharField(max_length=32)

    def __str__(self):
        return self.name
