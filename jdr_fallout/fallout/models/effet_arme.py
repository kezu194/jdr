from django.db import models


BRUTAL = "Brutal"
DEZONE = "De zone"
DESTRUCTEUR = "Destructeur"
ENRAFALE = "En rafale"
ETOURDISSANT = "Étourdissant"
PERFORANT = "Perforant"
PERSISTANT = "Persistant"
RADIOACTIF = "Radioactif"

LIST_EFFET_ARME = (
    BRUTAL,
    DEZONE,
    DESTRUCTEUR,
    ENRAFALE,
    ETOURDISSANT,
    PERFORANT,
    PERSISTANT,
    RADIOACTIF
)


class EffetArme(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self) -> str:
        return self.name
