from django.db import models

from fallout.models.personnage import Personnage


class AptitudeModel(models.Model):
    """
    Aptitude model with all the fields.
    """
    name = models.CharField(max_length=100)
    nb_rangs = models.IntegerField(default=1)
    prerequis = models.JSONField()
    description = models.TextField(max_length=500)

    def __str__(self):
        return f"{self.name} [{self.nb_rangs}]"


class Aptitude(AptitudeModel):
    """
    Aptitude Class, to differ from the aptitude of the players.
    """


class AptitudeJoueur(AptitudeModel):
    """
    Aptitude class dedicated to a specific player.
    """
    proprietaire = models.ForeignKey(to=Personnage, on_delete=models.CASCADE)
    actual_rang = models.IntegerField(default=1)
