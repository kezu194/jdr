from typing import List, Dict, Any

from django.db import models
from django.core.validators import MinValueValidator
from fallout.models.campagne import Campagne

# Déclaration des noms des origines
CONFRERIE_ACIER = "Confrérie de l'acier"
GOULE = "Goule"
SUPER_MUTANT = "Super mutant"
MISTER_HANDY = "Mister handy"
SURVIVANT = "Survivant"
HABITANT_ABRIS = "Habitant de l'abri"

# List origine pour lister les choix possibles à la création
LIST_ORIGINE = (
    (1, CONFRERIE_ACIER),
    (2, GOULE),
    (3, SUPER_MUTANT),
    (4, MISTER_HANDY),
    (5, SURVIVANT),
    (6, HABITANT_ABRIS)
)


class Personnage(models.Model):
    """
    Class de tous les personnages, joueurs et pnj.
    """
    name = models.CharField(max_length=200)
    origine = models.IntegerField(choices=LIST_ORIGINE, null=True)
    quest = models.ForeignKey(to=Campagne, null=True, blank=True, on_delete=models.CASCADE, default=None)
    level = models.IntegerField(default=1)
    xp = models.IntegerField(default=0)

    # S.P.E.C.I.A.L
    force = models.IntegerField(default=6, validators=[MinValueValidator(4)])
    perception = models.IntegerField(default=6, validators=[MinValueValidator(4)])
    endurance = models.IntegerField(default=6, validators=[MinValueValidator(4)])
    charisme = models.IntegerField(default=6, validators=[MinValueValidator(4)])
    intelligence = models.IntegerField(default=6, validators=[MinValueValidator(4)])
    agilite = models.IntegerField(default=6, validators=[MinValueValidator(4)])
    chance = models.IntegerField(default=6, validators=[MinValueValidator(4)])
    chance_point = models.IntegerField(default=0, validators=[MinValueValidator(4)])

    # PV max = Chance + Endurance
    pv_max = models.IntegerField(default=0)
    pv = models.IntegerField(default=0)

    # Bonus dégâts du joueur avec armes de corps à corps
    # Force 7-8 = 1
    # Force 9-10 = 2
    # Force 11+ = 3
    bonus_corps_a_corps = models.IntegerField(default=0)

    # Initiative = perception + agilite
    initiative = models.IntegerField(default=0)

    # Bonus defense
    # agilite <= 8 : 1
    # agilite >= 9 : 2
    bonus_defense = models.IntegerField(default=0)

    # Charge maximal = 75 + force x 5
    charge_maximal = models.IntegerField(default=0)

    # Résistance global au poison, ne concerne pas un membre en particulier
    resistance_poison = models.IntegerField(default=0)

    def __str__(self):
        return self.name

    @property
    def get_pourcentage_health(self) -> float:
        """
        Get the pourcentage of health the player has remaining.

        :return: The pourcentage of health as (PV/MAX) * 100
        """
        return (self.pv / self.pv_max) * 100

    @property
    def get_attributes(self):
        attributes: Dict[str, Any] = {
            "S": self.force,
            "P": self.perception,
            "E": self.endurance,
            "C": self.charisme,
            "I": self.intelligence,
            "A": self.agilite,
        }
        return attributes

    @property
    def is_the_biggest(self) -> List[str]:
        """
        Get the list of the biggest attributes of a personnage.

        :return: The list of attribute that have the biggest value
        """
        attributes = self.get_attributes
        max_attributes = []
        max_value = 0
        for letter, attribute in attributes.items():
            if attribute > max_value:
                max_attributes = [letter]
                max_value = attribute
            elif attribute == max_value:
                max_attributes.append(letter)
        return max_attributes

    @property
    def get_experience_needed(self):
        """
        Calcul la quantité d'XP nécessaire au joueur pour monter de niveau.

        La quantité nécessaire est calculé par le calcul:
        (Niveau à atteindre x (Niveau actuel / 2)) x 100

        1 -> 2 : 100
        2 -> 3 : 300
        3 -> 4 : 600
        """
        actual_level = self.level
        next_level = actual_level + 1
        return (next_level * (actual_level / 2)) * 100


class Parti(models.Model):
    """
    Classe Partie, représente l'état d'une partie du
    corp d'un personnage (tête, bras, jambe et buste)
    Par défault, tous les membres ont une résistance de
    0 pour tous les types de dégâts.
    L'ajout d'armure/Tenue peut changer leurs valeurs.
    """

    class Position(models.IntegerChoices):
        TETE = 1
        BUSTE = 2
        BRAS_GAUCHE = 3
        BRAS_DROITE = 4
        JAMBE_GAUCHE = 5
        JAMBE_DROITE = 6

    personnage = models.ForeignKey(Personnage, on_delete=models.CASCADE)
    position = models.IntegerField(choices=Position.choices)
    resistance_balistique = models.IntegerField(default=0)
    resistance_energie = models.IntegerField(default=0)
    resistance_radiation = models.IntegerField(default=0)

    def __str__(self) -> str:
        return f"{self.personnage.name} {self.get_position_display()}"


class Competence(models.Model):
    """
    Classe Compétence qui décrit la liste des compétences de tout les personnages.

    C'est cette liste qui va décrire l'efficacité des joueurs à effectuer certaines actions.
    Ex: Arme à Energie (PERCEPTION) : 2 
    Cela signifie que la compétence d'arme à énergie dépendant de l'attribut Perception
    et que le niveau de celui-ci est de niveau 2
    """

    class AttributDisplay(models.TextChoices):
        FORCE = "FOR"
        PERCEPTION = "PER"
        ENDURANCE = "END"
        CHARISME = "CHR"
        INTELLIGENCE = "INT"
        AGILITE = "AGI"

    class CompetenceName(models.TextChoices):
        ARME_ENERGIE = "Armes à énergie"
        ARME_CORPS = "Armes de corps à corps"
        ARME_LEGERES = "Armes légères"
        ARMES_LOURDES = "Armes lourdes"
        ATHLETISME = "Athlétisme"
        CROCHETAGE = "Crochetage"
        DISCOURS = "Discours"
        DISCRETION = "Discrétion"
        EXPLOSIFS = "Explosifs"
        MAIN_NUES = "Mains nues"
        MEDECINE = "Médecine"
        PILOTAGE = "Pilotage"
        PROJECTILES = "Projectiles"
        REPARATION = "Réparation"
        SCIENCES = "Sciences"
        SURVIE = "Survie"
        TROC = "Troc"
        OTHER = "Other"

    ASSOCIATION = {
        CompetenceName.ARME_ENERGIE: AttributDisplay.PERCEPTION,
        CompetenceName.ARME_CORPS: AttributDisplay.FORCE,
        CompetenceName.ARME_LEGERES: AttributDisplay.AGILITE,
        CompetenceName.ARMES_LOURDES: AttributDisplay.ENDURANCE,
        CompetenceName.ATHLETISME: AttributDisplay.FORCE,
        CompetenceName.CROCHETAGE: AttributDisplay.PERCEPTION,
        CompetenceName.DISCOURS: AttributDisplay.CHARISME,
        CompetenceName.DISCRETION: AttributDisplay.AGILITE,
        CompetenceName.EXPLOSIFS: AttributDisplay.PERCEPTION,
        CompetenceName.MAIN_NUES: AttributDisplay.FORCE,
        CompetenceName.MEDECINE: AttributDisplay.INTELLIGENCE,
        CompetenceName.PILOTAGE: AttributDisplay.PERCEPTION,
        CompetenceName.PROJECTILES: AttributDisplay.AGILITE,
        CompetenceName.REPARATION: AttributDisplay.INTELLIGENCE,
        CompetenceName.SCIENCES: AttributDisplay.INTELLIGENCE,
        CompetenceName.SURVIE: AttributDisplay.ENDURANCE,
        CompetenceName.TROC: AttributDisplay.CHARISME,
    }

    personnage = models.ForeignKey(Personnage, on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=50, choices=CompetenceName.choices)
    attribut = models.CharField(max_length=3, choices=AttributDisplay.choices, null=True)
    is_atout_perso = models.BooleanField(default=False)
    rang = models.IntegerField(default=0)

    def __str__(self) -> str:
        return f"{self.personnage.name}: {self.name} [{self.rang}]"
