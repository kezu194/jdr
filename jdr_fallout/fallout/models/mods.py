from fallout.models.effet import Effet
from fallout.models.item import Item
from fallout.models.arme import Arme, ArmeLootable
from fallout.models.personnage import Personnage
from django.db import models


class EmplacementMods(models.TextChoices):
    # Classique
    CULASSE = "Culasse"
    CANON = "Canon"
    CHARGEUR = "Chargeur"
    POIGNEE = "Poignée"
    CROSSE = "Crosse"
    VISEUR = "Viseur"
    BOUCHE = "Bouche"

    # Arme Énergétique
    CONDENSATEUR = "Condensateur"
    PARABOLE = "Parabole"

    # Arme Lourde
    CARBURANT = "Carburant"
    RESERVOIR_PROPERGOL = "Réservoir à propergol"
    BUSE = "Buse"

    # Arme de corps à corps
    LAME = "Lame"
    ARME_POING = "Arme de poing"

    AUTRE = "Autre"


class ModelMods(Item):
    emplacement = models.CharField(max_length=100, choices=EmplacementMods.choices)
    compatible_arme = models.ManyToManyField(to=ArmeLootable, blank=True)
    effet = models.ForeignKey(to=Effet, on_delete=models.CASCADE, null=True,
                              blank=True)
    description_effet = models.CharField(max_length=200, blank=True, null=True)

    def __str__(self):
        return super().__str__()


class ModsLootable(ModelMods):
    def __str__(self):
        return super().__str__()


class Mods(Item):
    proprietaire = models.ForeignKey(Personnage, on_delete=models.CASCADE)
    assigned_to = models.ForeignKey(Arme, blank=True, null=True,
                                    on_delete=models.SET_NULL)
    based_on = models.ForeignKey(ModsLootable, on_delete=models.CASCADE)

    def __str__(self):
        return super().__str__()
