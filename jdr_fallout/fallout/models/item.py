from django.db import models
from django.core.validators import MinValueValidator


class Item(models.Model):
    name = models.CharField(max_length=100, null=True)
    poid = models.FloatField(validators=[MinValueValidator(0)], null=True)
    prix = models.IntegerField(validators=[MinValueValidator(0)], null=True)

    def __str__(self):
        return self.name

    class Meta:
        abstract = True
