from django.db import models
from django.core.validators import MinValueValidator
from fallout.models.personnage import Personnage
from fallout.models.item import Item


class ModelMunition(Item):
    rarete = models.IntegerField(validators=[MinValueValidator(0)])

    # Indique la quantite minimum de munition de ce type trouvé lors d'une fouille
    quantite_trouve = models.IntegerField(validators=[MinValueValidator(1)])

    # Indique le ratio de munition trouvé en bonus en fonction du jet du joueur
    ratio_munition_trouve = models.IntegerField(validators=[MinValueValidator(1)])

    def __str__(self) -> str:
        return self.name


class MunitionLootable(ModelMunition):

    def __str__(self) -> str:
        return super().__str__()


class Munition(ModelMunition):
    proprietaire = models.ForeignKey(Personnage, on_delete=models.CASCADE)
    quantite = models.IntegerField(default=0, validators=[MinValueValidator(0)])

    def __str__(self) -> str:
        return f"{super().__str__()} de {self.proprietaire}"
