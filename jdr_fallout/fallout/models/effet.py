from fallout.models.arme import ArmeDistante, Arme, TypeDegat
from fallout.services.service_arme import ServiceArme
from fallout.models.qualites import (
    IMPRECIS, FIABLE, PRECIS, RECO, DEUX_MAINS, ZONE_IMPACT,
    COMBAT_RAPPROCHE, IMPREVISIBLE, SILENCIEUX, VISION_NOCTURNE)
from fallout.models.effet_arme import (
    ENRAFALE, RADIOACTIF, DEZONE, ETOURDISSANT, BRUTAL, PERSISTANT,
    PERFORANT
)

from django.db import models


class Effet(models.Model):
    name = models.CharField(max_length=100)

    def use_effect(self, arme: Arme) -> None:
        return

    def undo_effect(self, arme: Arme) -> None:
        return

    def __str__(self):
        return self.name


class EffetDegatPlus1(Effet):

    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", 1)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", -1)


class EffetDegatPlus2(Effet):

    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", 2)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", -2)


class EffetDegatPlus3(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", 3)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", -3)


class EffetDegatPlus3CadencePlus1(Effet):

    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", 3)
        service.add_in_attribute("cadence", 1)

    def undo_effect(self, arme: ArmeDistante) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", -3)
        service.add_in_attribute("cadence", -1)


class EffetBrutal(Effet):

    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_effet(BRUTAL)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_effet(BRUTAL)


class EffetAutomatique(Effet):

    def use_effect(self, arme: ArmeDistante) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", -1)
        service.add_in_attribute("cadence", 2)
        service.add_effet(ENRAFALE)
        service.add_qualite(IMPRECIS)

    def undo_effect(self, arme: ArmeDistante) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", 1)
        service.add_in_attribute("cadence", -2)
        service.set_base_effet(ENRAFALE)
        service.set_base_qualite(IMPRECIS)


class EffetCadencePlus1(Effet):

    def use_effect(self, arme: ArmeDistante) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("cadence", 1)

    def undo_effect(self, arme: ArmeDistante) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("cadence", -1)


class EffetCartouche38(Effet):

    def use_effect(self, arme: ArmeDistante) -> None:
        service = ServiceArme(arme)
        service.change_munition("Cartouche .38")
        service.set_attribute("degat", 4)

    def undo_effect(self, arme: ArmeDistante) -> None:
        service = ServiceArme(arme)
        service.set_base_munition()
        service.set_base_attribute("degat")


class EffetCartouche308(Effet):

    def use_effect(self, arme: ArmeDistante) -> None:
        service = ServiceArme(arme)
        service.change_munition("Cartouche .308")
        service.set_attribute("degat", 7)

    def undo_effect(self, arme: ArmeDistante) -> None:
        service = ServiceArme(arme)
        service.set_base_munition()
        service.set_base_attribute("degat")


class EffetCartouche45(Effet):

    def use_effect(self, arme: ArmeDistante) -> None:
        service = ServiceArme(arme)
        service.change_munition("Cartouche .45")
        service.set_attribute("degat", 4)
        service.add_in_attribute("cadence", 1)

    def undo_effect(self, arme: ArmeDistante) -> None:
        service = ServiceArme(arme)
        service.set_base_munition()
        service.set_base_attribute("degat")
        service.add_in_attribute("cadence", -1)


class EffetCalibre50(Effet):

    def use_effect(self, arme: ArmeDistante) -> None:
        service = ServiceArme(arme)
        service.change_munition("Calibre .50")
        service.set_attribute("degat", 8)
        service.add_effet(BRUTAL)

    def undo_effect(self, arme: ArmeDistante) -> None:
        service = ServiceArme(arme)
        service.set_base_munition()
        service.set_base_attribute("degat")
        service.set_base_effet(BRUTAL)


class EffetCadencePlus2PorteeMoins1(Effet):
    def use_effect(self, arme: ArmeDistante) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("cadence", 2)
        service.change_portee(-1)

    def undo_effect(self, arme: ArmeDistante) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("cadence", -2)
        service.change_portee(1)


class EffetImprecis(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_qualite(IMPRECIS)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_qualite(IMPRECIS)


class EffetFiable(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_qualite(FIABLE)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_qualite(FIABLE)


class EffetPorteePlus1(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.change_portee(1)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.change_portee(-1)


class EffetPorteePlus1CadencePlus1(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.change_portee(1)
        service.add_in_attribute("cadence", 1)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.change_portee(-1)
        service.add_in_attribute("cadence", -1)


class EffetPorteePlus1CadencePlus1Fiable(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("cadence", 1)
        service.change_portee(1)
        service.add_qualite(FIABLE)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("cadence", -1)
        service.change_portee(-1)
        service.set_base_qualite(FIABLE)


class EffetCanonScie(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.remove_qualite(DEUX_MAINS)
        service.add_qualite(COMBAT_RAPPROCHE)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_qualite(DEUX_MAINS)
        service.set_base_qualite(COMBAT_RAPPROCHE)


class EffetDegatPlus1PorteePlus1(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", 1)
        service.change_portee(1)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", -1)
        service.change_portee(-1)


class EffetCadencePlus1Imprevisible(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("cadence", 1)
        service.add_qualite(IMPREVISIBLE)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("cadence", -1)
        service.set_base_qualite(IMPREVISIBLE)


class EffetPerdImprecis(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.remove_qualite(IMPRECIS)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_qualite(IMPRECIS)


class EffetPerdImprecisPerforantPlus1(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.remove_qualite(IMPRECIS)
        service.add_effet(PERFORANT)
        service.add_in_attribute("perforation", 1)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_qualite(IMPRECIS)
        service.set_base_effet(PERFORANT)
        service.add_in_attribute("perforation", -1)


class EffetCrosseComplete(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_qualite(DEUX_MAINS)
        service.remove_qualite(IMPRECIS)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_qualite(DEUX_MAINS, IMPRECIS)


class EffetTireurElite(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_qualite(DEUX_MAINS, PRECIS)
        service.remove_qualite(IMPRECIS)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_qualite(DEUX_MAINS, PRECIS, IMPRECIS)


class EffetCrosseCompensateur(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_qualite(DEUX_MAINS)
        service.remove_qualite(IMPRECIS)
        service.add_in_attribute("cadence", 1)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_qualite(DEUX_MAINS, IMPRECIS)
        service.add_in_attribute("cadence", -1)


class EffetRelancerJetLocalisation(Effet):
    """
    TODO Permettre de pouvoir faire relancer le jet de
      localisation avec cet effet
    """


class EffetPrecis(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_qualite(PRECIS)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_qualite(PRECIS)


class EffetPrecisPorteePlus1(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_qualite(PRECIS)
        service.change_portee(1)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_qualite(PRECIS)
        service.change_portee(-1)


class EffetPrecisVisionNocturne(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_qualite(PRECIS, VISION_NOCTURNE)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_qualite(PRECIS, VISION_NOCTURNE)


class EffetPrecisVisionNocturnePorteePlus1(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_qualite(PRECIS, VISION_NOCTURNE)
        service.change_portee(1)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_qualite(PRECIS, VISION_NOCTURNE)
        service.change_portee(-1)


class EffetPrecisReco(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_qualite(PRECIS, RECO)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_qualite(PRECIS, RECO)


class EffetBaionnette(Effet):
    def use_effect(self, arme: Arme) -> None:
        return
        # TODO pouvoirs intégrer l'utilisation d'une arme
        #  à distance en arme de mêlé

    def undo_effect(self, arme: Arme) -> None:
        return


class EffetPerdImprecisCadencePlus1(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.remove_qualite(IMPRECIS)
        service.add_in_attribute("cadence", 1)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_qualite(IMPRECIS)
        service.add_in_attribute("cadence", -1)


class EffetSilencieux(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_qualite(SILENCIEUX)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_qualite(SILENCIEUX)


# Effet Arme laser


class Effet3Charges(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_attribute("nombre_munition_par_tir", 3)
        service.add_in_attribute("degat", 1)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_attribute("nombre_munition_par_tir")
        service.add_in_attribute("degat", -1)


class Effet4Charges(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_attribute("nombre_munition_par_tir", 4)
        service.add_in_attribute("degat", 2)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_attribute("nombre_munition_par_tir")
        service.add_in_attribute("degat", -2)


class Effet5Charges(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_attribute("nombre_munition_par_tir", 5)
        service.add_in_attribute("degat", 3)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_attribute("nombre_munition_par_tir")
        service.add_in_attribute("degat", -3)


class Effet6Charges(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_attribute("nombre_munition_par_tir", 6)
        service.add_in_attribute("degat", 4)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_attribute("nombre_munition_par_tir")
        service.add_in_attribute("degat", -4)


class EffetElectrique(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_attribute("degat", 7)
        service.set_attribute("type_degat", TypeDegat.ENERGETIQUE)
        service.add_qualite(RADIOACTIF)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_attribute("degat", "type_degat")
        service.set_base_qualite(RADIOACTIF)


# Mods d'armes à energies

class EffetAuto(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("cadence", 2)
        service.add_effet(ENRAFALE)
        service.remove_qualite(ZONE_IMPACT)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("cadence", -2)
        service.set_base_effet(ENRAFALE)
        service.set_base_qualite(ZONE_IMPACT)


class EffetPersistant(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_effet(PERSISTANT)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_effet(PERSISTANT)


class EffetDegatPlus1CadenceMoins1(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", 1)
        service.add_in_attribute("cadence", -1)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", -1)
        service.add_in_attribute("cadence", 1)


class EffetBrutalDegatPlus1(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_effet(BRUTAL)
        service.add_in_attribute("degat", 1)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_effet(BRUTAL)
        service.add_in_attribute("degat", -1)


class EffetBouche(Effet):
    # TODO Permettre d'autoriser à l'arme un mod de bouche (gérer la logique)
    def use_effect(self, arme: Arme) -> None:
        ...

    def undo_effect(self, arme: Arme) -> None:
        ...


class EffetCombatRapprochePorteePlus1(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.remove_qualite(COMBAT_RAPPROCHE)
        service.change_portee(1)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_qualite(COMBAT_RAPPROCHE)
        service.change_portee(-1)


class EffetScattergun(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", -1)
        service.add_effet(DEZONE)
        service.add_qualite(IMPRECIS)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", 1)
        service.set_base_effet(DEZONE)
        service.set_base_qualite(IMPRECIS)


class EffetCanonAuto(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", -1)
        service.add_in_attribute("cadence", 1)
        service.remove_qualite(COMBAT_RAPPROCHE)
        service.change_portee(1)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", 1)
        service.add_in_attribute("cadence", -1)
        service.set_base_qualite(COMBAT_RAPPROCHE)
        service.change_portee(-1)


class EffetCanonLong(Effet):
    # TODO idem que le dernier TODO
    def use_effect(self, arme: Arme) -> None:
        ...

    def undo_effect(self, arme: Arme) -> None:
        ...


class EffetCanonSniper(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", 2)
        service.remove_qualite(COMBAT_RAPPROCHE)
        service.change_portee(1)
        service.add_in_attribute("cadence", -1)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", -2)
        service.set_base_qualite(COMBAT_RAPPROCHE)
        service.change_portee(-1)
        service.add_in_attribute("cadence", 1)


class EffetLanceFlamme(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", -2)
        service.add_in_attribute("cadence", 2)
        service.add_effet(DEZONE)
        service.add_effet(ENRAFALE)
        service.change_portee(-1)
        service.add_qualite(IMPRECIS)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_attribute("degat", "cadence")
        service.set_base_effet(DEZONE, ENRAFALE)
        service.change_portee(1)
        service.set_base_qualite(IMPRECIS)


class EffetDeuxMainsImprecisCombatRapproche(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_qualite(DEUX_MAINS)
        service.remove_qualite(COMBAT_RAPPROCHE, IMPRECIS)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_qualite(DEUX_MAINS, COMBAT_RAPPROCHE, IMPRECIS)


class EffetPerforant(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_effet(PERFORANT)
        service.add_in_attribute("perforation", 1)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_effet(PERFORANT)
        service.add_in_attribute("perforation", -1)


class EffetPerforantCombatRapproche(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_effet(PERFORANT)
        service.add_in_attribute("perforation", 1)
        service.remove_qualite(COMBAT_RAPPROCHE)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_effet(PERFORANT)
        service.add_in_attribute("perforation", -1)
        service.set_base_qualite(COMBAT_RAPPROCHE)


class EffetTireurEliteV2(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_qualite(DEUX_MAINS, PRECIS)
        service.remove_qualite(IMPRECIS, COMBAT_RAPPROCHE)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_qualite(
            DEUX_MAINS, PRECIS,
            IMPRECIS, COMBAT_RAPPROCHE
        )


class EffetCompensationRecul(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_qualite(DEUX_MAINS)
        service.remove_qualite(IMPRECIS, COMBAT_RAPPROCHE)
        service.add_in_attribute("cadence", 1)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_qualite(DEUX_MAINS, IMPRECIS, COMBAT_RAPPROCHE)
        service.add_in_attribute("cadence", -1)


class EffetDispersion(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", -1)
        service.add_in_attribute("cadence", -1)
        service.add_effet(DEZONE)
        service.add_qualite(IMPRECIS)
        service.change_portee(-1)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", 1)
        service.add_in_attribute("cadence", 1)
        service.set_base_effet(DEZONE)
        service.set_base_qualite(IMPRECIS)
        service.change_portee(1)


class EffetCadencePlus1Imprecis(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("cadence", 1)
        service.remove_qualite(IMPRECIS)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("cadence", -1)
        service.set_base_qualite(IMPRECIS)


class EffetBrutalEnergetique(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_effet(BRUTAL)
        service.set_attribute("type_degat", TypeDegat.ENERGETIQUE)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_effet(BRUTAL)
        service.set_base_attribute("type_degat")


class EffetPersistanceEnergetique(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_effet(PERSISTANT)
        service.set_attribute("type_degat_persistance", TypeDegat.ENERGETIQUE)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_effet(PERSISTANT)
        service.set_base_attribute("type_degat_persistance")


class EffetCadencePlus2(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("cadence", 2)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("cadence", -2)


class EffetIgnoreAbri(Effet):
    # TODO permettre d'ignorer la cover d'une cible
    def use_effect(self, arme: Arme) -> None:
        return

    def undo_effect(self, arme: Arme) -> None:
        return


class PerforantPlus1(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_effet(PERFORANT)
        service.add_in_attribute("perforation", 1)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_effet(PERFORANT)
        service.add_in_attribute("perforation", -1)


class DegatPlus1Energetique(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", 1)
        service.set_attribute("type_degat", TypeDegat.ENERGETIQUE)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", -1)
        service.set_base_attribute("type_degat")


class EffetDentElectrique(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", 1)
        service.set_attribute("type_degat", TypeDegat.ENERGETIQUE)
        service.add_effet(PERSISTANT)
        service.set_attribute("type_degat_persistance", TypeDegat.BALISTIQUE)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", -1)
        service.set_base_attribute("type_degat", "type_degat_persistance")
        service.set_base_effet(PERSISTANT)


class EffetEtourdissant(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", 2)
        service.set_attribute("type_degat", TypeDegat.ENERGETIQUE)
        service.add_effet(ETOURDISSANT)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", -2)
        service.set_base_attribute("type_degat")
        service.set_base_effet(ETOURDISSANT)


class EffetDegatPlus1Persistant(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", 1)
        service.add_effet(PERSISTANT)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", -1)
        service.set_base_effet(PERSISTANT)


class EffetDegatPlus1PersistantFurtif(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", 1)
        service.add_effet(PERSISTANT)
        service.add_in_attribute("bonus_furtivite", 2)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", -1)
        service.set_base_effet(PERSISTANT)
        service.add_in_attribute("bonus_furtivite", -2)


class EffetLameFurtive(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", 2)
        service.add_effet(PERSISTANT)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", -2)
        service.set_base_effet(PERSISTANT)


class EffetLameCourbe(Effet):
    # TODO Donner l'option de désarmer l'adversaire avec cet effet
    def use_effect(self, arme: Arme) -> None:
        return

    def undo_effect(self, arme: Arme) -> None:
        return


class EffetPerforantDegatPlus1(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", 1)
        service.add_effet(PERFORANT)
        service.add_in_attribute("perforation", 1)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", -1)
        service.set_base_effet(PERFORANT)
        service.add_in_attribute("perforation", -1)


class EffetDegatPlus2Perforant(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", 2)
        service.add_effet(PERFORANT)
        service.add_in_attribute("perforation", 1)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", -2)
        service.set_base_effet(PERFORANT)
        service.add_in_attribute("perforation", -1)


class EffetDegatPlus1Energetique(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", 1)
        service.set_attribute("type_degat", TypeDegat.ENERGETIQUE)


class EffetDegatPlus2Energetique(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", 2)
        service.set_attribute("type_degat", TypeDegat.ENERGETIQUE)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", -2)
        service.set_base_attribute("type_degat")


class EffetDegatPlus3Energetique(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", 3)
        service.set_attribute("type_degat", TypeDegat.ENERGETIQUE)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", -3)
        service.set_base_attribute("type_degat")


class EffetCharge(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", 4)
        service.add_in_attribute("cadence", -3)
        service.change_portee(1)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", -4)
        service.add_in_attribute("cadence", 3)
        service.change_portee(-1)


class PerforantPorteePlus1(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_effet(PERFORANT)
        service.add_in_attribute("perforation", 1)
        service.change_portee(1)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.set_base_effet(PERFORANT)
        service.add_in_attribute("perforation", -1)
        service.change_portee(-1)


class EffetGrandeVitesse(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", 1)
        service.add_in_attribute("cadence", 1)
        service.change_portee(-1)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", -1)
        service.add_in_attribute("cadence", -1)
        service.change_portee(1)


class EffetGrandePuissance(Effet):
    def use_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", 2)
        service.add_in_attribute("cadence", -2)

    def undo_effect(self, arme: Arme) -> None:
        service = ServiceArme(arme)
        service.add_in_attribute("degat", -2)
        service.add_in_attribute("cadence", 2)
