from django.db import models

COMBAT_RAPPROCHE = "Combat rapproché"
DEUX_MAINS = "Deux mains"
DISSIMULE = "Dissimulé"
FIABLE = "Fiable"
GATLING = "Gatling"
IMPRECIS = "Imprécis"
IMPREVISIBLE = "Imprévisible"
INVALIDANT = "Invalidant"
LANCER_C = "Lancer (C)"
LANCER_M = "Lancer (M)"
MINE = "Mine"
PARADE = "Parade"
PRECIS = "Précis"
RECO = "Reco"
SILENCIEUX = "Silencieux"
VISION_NOCTURNE = "Vision nocturne"
ZONE_IMPACT = "Zone d'impact"

LIST_QUALITE_ARME = (
    COMBAT_RAPPROCHE,
    DEUX_MAINS,
    DISSIMULE,
    FIABLE,
    GATLING,
    IMPRECIS,
    IMPREVISIBLE,
    INVALIDANT,
    LANCER_C,
    LANCER_M,
    MINE,
    PARADE,
    PRECIS,
    RECO,
    SILENCIEUX,
    VISION_NOCTURNE,
    ZONE_IMPACT
)


class Qualites(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self) -> str:
        return self.name
