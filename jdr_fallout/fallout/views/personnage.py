from django.shortcuts import render, redirect
from fallout.models import Personnage, Competence, Parti, ArmeDistante, Arme, TypeArme, Mods, Aptitude
from fallout.models.personnage import LIST_ORIGINE
from fallout.forms import PersonnageForm
from django.forms import modelformset_factory

from fallout.services import ServicePersonnage


def list_all_personnage(request):
    context = {"personnages": Personnage.objects.all()}
    return render(request=request, template_name="personnage/list_personnage.html", context=context)


def display_personnage(request, pk):
    personnage = Personnage.objects.get(pk=pk)
    list_competences = Competence.objects.filter(personnage=personnage)
    list_attributes = [
        personnage.force,
        personnage.perception,
        personnage.endurance,
        personnage.charisme,
        personnage.intelligence,
        personnage.agilite,
        personnage.chance
    ]
    list_parti = Parti.objects.filter(personnage=personnage)
    list_arme_distante = ArmeDistante.objects.filter(proprietaire=personnage)

    type_melee = [TypeArme.CORPSACORPS, TypeArme.EXPLOSIF, TypeArme.PROJECTILE]
    list_arme_melee = Arme.objects.filter(proprietaire=personnage, type__in=type_melee)
    list_mods = Mods.objects.filter(proprietaire=personnage)

    context = {
        "personnage": personnage,
        "list_competences": list_competences,
        "list_parti": list_parti,
        "list_arme_distante": list_arme_distante,
        "list_arme_melee": list_arme_melee,
        "list_mods": list_mods,
        "list_attributes": list_attributes
    }
    return render(request, 'personnage/display_personnage.html', context)


def create_personnage(request):
    if request.method == "POST":
        ServicePersonnage().create_new_personnage(request)
        return render(request, 'personnage/list_personnage.html', {'personnages': Personnage.objects.all()})

    form = PersonnageForm()
    aptitudes = {aptitude.id: {'name': aptitude.name, 'nb_rangs': aptitude.nb_rangs, 'prerequis': aptitude.prerequis,
                               'description': aptitude.description} for aptitude in Aptitude.objects.all()}
    origines = [origine for id, origine in LIST_ORIGINE]
    context = {'form': form, "competences": Competence.ASSOCIATION, "aptitudes": aptitudes, "origines": origines}
    return render(request=request, template_name="personnage/create_personnage.html", context=context)


def update_personnage(request, pk):
    personnage = Personnage.objects.get(pk=pk)
    competence_form_set = modelformset_factory(Competence, fields=['name', 'attribut', 'is_atout_perso', 'rang'], extra=0)

    if request.method == "POST":
        persoform = PersonnageForm(request.POST, instance=personnage)
        compform = competence_form_set(request.POST, queryset=Competence.objects.filter(personnage=personnage))
        for comp in compform:
            comp.fields["name"].disabled = True
            comp.fields["attribut"].disabled = True
        if all([persoform.is_valid(), compform.is_valid()]):
            personnage: Personnage = persoform.save()
            for c in compform:
                c.save()
            personnage.pv = personnage.endurance + personnage.chance
            match personnage.force:
                case num if 7 <= num <= 8:
                    personnage.bonus_corps_a_corps = 1
                case num if 9 <= num <= 10:
                    personnage.bonus_corps_a_corps = 2
                case num if num >= 11:
                    personnage.bonus_corps_a_corps = 3
            personnage.initiative = personnage.perception + personnage.agilite
            personnage.bonus_defense = 1 if personnage.agilite <= 8 else 2
            personnage.charge_maximal = 75 + personnage.force * 5
            personnage.chance_point = personnage.chance
            personnage.save()
            return redirect("/personnage/" + str(pk))
        else:
            print(persoform.errors.as_data())
            print(compform.errors)
            print(compform.error_messages)

    persoform = PersonnageForm(instance=personnage)
    compform = competence_form_set(queryset=Competence.objects.filter(personnage=personnage))
    for comp in compform:
        comp.fields["name"].disabled = True
        comp.fields["attribut"].disabled = True

    return render(request=request, template_name="personnage/modify_personnage.html",
                  context={'persoform': persoform, 'compform': compform})
