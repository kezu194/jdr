from django.shortcuts import render
from fallout.models import Personnage, Competence, Campagne


def list_all_campagne(request):
    context = {"list_campagne": Campagne.objects.all()}
    return render(request=request, template_name="campagne/list_campagne.html", context=context)


def display_campagne(request, pk):
    campagne = Campagne.objects.get(id=pk)
    list_perso = Personnage.objects.filter(quest=campagne)
    context = {
        "campagne": campagne,
        "list_perso": list_perso
    }
    return render(request, 'campagne/display_campagne.html', context)
