from django.shortcuts import redirect, render
from django.contrib.auth import logout
from django.contrib.auth.forms import AuthenticationForm


def login(request):
    context = {}
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            redirect("/")
    else:
        context['form'] = AuthenticationForm()
    return render(request=request, template_name="login.html", context=context)

def logout_view(request):
    logout(request)
    return redirect("login")
