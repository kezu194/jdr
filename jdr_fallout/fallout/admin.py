from django.contrib import admin
from .models import Personnage, Competence, Campagne, Arme, \
    Munition, Mods, ArmeLootable, MunitionLootable, \
    ModsLootable, ArmeDistante, ArmeDistanteLootable, Aptitude, AptitudeJoueur

# Personnage model
admin.site.register(Personnage)
admin.site.register(Competence)

# Campagne model
admin.site.register(Campagne)

# Arme model
admin.site.register(Arme)
admin.site.register(ArmeDistante)
admin.site.register(ArmeLootable)
admin.site.register(ArmeDistanteLootable)

# Munition model
admin.site.register(MunitionLootable)
admin.site.register(Munition)

# Mods model
admin.site.register(ModsLootable)
admin.site.register(Mods)

# Aptitude
admin.site.register(Aptitude)
admin.site.register(AptitudeJoueur)
