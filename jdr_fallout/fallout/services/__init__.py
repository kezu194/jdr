from fallout.services.service_arme import ServiceArme
from fallout.services.service_personnage import ServicePersonnage

__all__ = [
    ServiceArme,
    ServicePersonnage
]
