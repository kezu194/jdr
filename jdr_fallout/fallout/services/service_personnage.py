from fallout.models import Personnage, Aptitude, AptitudeJoueur
from fallout.models.personnage import LIST_ORIGINE, Parti, Competence


class ServicePersonnage:
    """
    Service class dedicated on the Personnage model.
    """

    def create_new_personnage(self, request):
        """
        Realize the creation of a new Personnage based on the request received and the completed form.

        :param request: The post request of the form
        :return: the new Personnage object
        """

        def get_origine_number(origine_name: str) -> int:
            """
            Get the origine number from the list of origines

            :param origine_name:
            :return:
            """
            origines = [origine for id, origine in LIST_ORIGINE]
            for id, origine in enumerate(origines):
                if origine == origine_name:
                    return id + 1

        name: str = request.POST["nom"]
        origine_number: int = get_origine_number(request.POST["origine-choice"])

        force: int = int(request.POST["force"])
        perception: int = int(request.POST["perception"])
        endurance: int = int(request.POST["endurance"])
        charisme: int = int(request.POST["charisme"])
        intelligence: int = int(request.POST["intelligence"])
        agilite: int = int(request.POST["agilite"])
        chance: int = int(request.POST["chance"])

        pv: int = endurance + chance
        degat_cc: int = 0
        match force:
            case num if 7 <= num <= 8:
                degat_cc = 1
            case num if 9 <= num <= 10:
                degat_cc = 2
            case num if num >= 11:
                degat_cc = 3
        initiative: int = perception + agilite
        defense: int = 1 if agilite <= 8 else 2
        charge_max: int = 75 + 5 * force

        personnage: Personnage = Personnage.objects.create(
            name=name,
            origine=origine_number,
            force=force,
            perception=perception,
            endurance=endurance,
            charisme=charisme,
            intelligence=intelligence,
            agilite=agilite,
            chance=chance,
            chance_point=chance,
            pv=pv,
            pv_max=pv,
            bonus_corps_a_corps=degat_cc,
            initiative=initiative,
            bonus_defense=defense,
            charge_maximal=charge_max,
        )

        # Attribution of Parties
        for position in list(Parti.Position):
            Parti.objects.create(personnage=personnage, position=position)

        # Attribution of Competence
        nb_competence: int = 0
        for competence, attribut in Competence.ASSOCIATION.items():
            atout: bool = True if request.POST.get(f"atout-{nb_competence}") else False
            rang: int = int(request.POST.get(f"rang-{nb_competence}"))
            Competence.objects.create(
                personnage=personnage, name=competence, attribut=attribut, is_atout_perso=atout, rang=rang
            )
            nb_competence += 1

        # Attribution of Aptitude
        aptitudes = Aptitude.objects.all()
        list_choosed_aptitudes = []
        aptitude_1: str = request.POST.get("select-aptitude")
        aptitude_2: str = request.POST.get("select-aptitude2")
        for aptitude in aptitudes:
            if aptitude.id == int(aptitude_1):
                list_choosed_aptitudes.append(aptitude)
            elif aptitude_2 != "default" and aptitude.id == int(aptitude_2):
                list_choosed_aptitudes.append(aptitude)

        for aptitude in list_choosed_aptitudes:
            AptitudeJoueur.objects.create(
                proprietaire=personnage, actual_rang=1, name=aptitude.name, nb_rangs=aptitude.nb_rangs,
                prerequis=aptitude.prerequis, description=aptitude.description
            )
