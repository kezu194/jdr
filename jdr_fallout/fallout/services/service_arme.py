from typing import Any, Dict

from fallout.models.arme import Arme, ArmeDistante, ArmeDistanteLootable, Portee, Qualites, EffetArme
from fallout.selector.selector_arme import get_arme_effet, get_arme_qualite
from fallout.selector.selector_munition import get_munition_of_owner


class ServiceArme:
    """
    Service class dedicated to all Arme business logic.
    """

    def __init__(self, arme: Arme) -> None:
        self.arme = arme

    def add_effet(self, *names) -> None:
        """
        Add an effet from the effet list of the Arme.

        :param names:
        :return:
        """
        for name in names:
            self.arme.effet.add(get_arme_effet(name))
        self.arme.save()

    def remove_effet(self, *names) -> None:
        """
        Remove an effet from the effet list of the Arme.

        :param names:
        :return:
        """
        for name in names:
            self.arme.effet.remove(get_arme_effet(name))
        self.arme.save()

    def add_qualite(self, *names) -> None:
        """
        Add a qualitee in the qualitee list of the Arme.

        :param names:
        :return:
        """
        for name in names:
            self.arme.qualite.add(get_arme_qualite(name))
        self.arme.save()

    def remove_qualite(self, *names) -> None:
        """
        Remove a qualitee in the qualitee list of the Arme.

        :param names:
        :return:
        """
        for name in names:
            self.arme.qualite.remove(get_arme_qualite(name))
        self.arme.save()

    def set_base_qualite(self, *names):
        """
        Reset a specific qualitee of an arme from it's model.

        :param names:
        :return:
        """
        for name in names:
            qualite: Qualites = get_arme_qualite(name)
            if qualite in self.arme.based_on.qualite.all():
                self.arme.qualite.add(qualite)
            else:
                self.arme.qualite.remove(qualite)
        self.arme.save()

    def set_base_effet(self, *names):
        """
        Reset a specific qualitee of an arme from its model.

        :param names:
        :return:
        """
        for name in names:
            effet: EffetArme = get_arme_effet(name)
            if effet in self.arme.based_on.effet.all():
                self.arme.effet.add(effet)
            else:
                self.arme.effet.remove(effet)
        self.arme.save()

    def add_in_attribute(self, attribute_name: str, value: int) -> None:
        """
        Add or remove value from an attribute from the Arme.

        :param attribute_name:
        :param value:
        :return:
        """
        setattr(self.arme, attribute_name, (getattr(self.arme, attribute_name) + value))
        self.arme.save()

    def set_attribute(self, attribute_name: str, value: Any) -> None:
        """
        Set the value of an attribute to a fix value.

        :param attribute_name:
        :param value:
        :return:
        """
        setattr(self.arme, attribute_name, value)
        self.arme.save()

    def set_base_attribute(self, *names) -> None:
        """
        Reset the value of the given attribute to it's default value.

        :param names:
        :return:
        """
        for name in names:
            setattr(self.arme, name, getattr(self.arme.based_on, name))
        self.arme.save()

    def change_munition(self, new_munition_name: str) -> None:
        """
        Change the munition type of the Arme.

        :param new_munition_name:
        :return:
        """
        self.arme: ArmeDistante
        self.arme.munition_type = get_munition_of_owner(new_munition_name, self.arme.proprietaire)
        self.arme.save()

    def set_base_munition(self) -> None:
        """
        Set the munition type of the Arme to it's original model.

        :return:
        """
        base_arme: ArmeDistanteLootable = self.arme.based_on
        self.arme.munition_type = base_arme.munition_type
        self.arme.save()

    def change_portee(self, value) -> None:
        """
        Add or remove a level of Portee of the Arme.

        :param value : nombre de niveaux de portée à gagner ou perdre
        :return:
        """
        self.arme: ArmeDistante
        dict_portee: Dict[int, str] = {index: value[0] for index, value in enumerate(Portee.choices)}
        match self.arme.portee:
            case Portee.COURTE:
                actual_value = 0
            case Portee.MOYENNE:
                actual_value = 1
            case Portee.LONGUE:
                actual_value = 2
            case _:
                actual_value = 3
        if actual_value + value > 3:
            self.set_attribute("portee", Portee.EXTREME)
        elif actual_value + value < 0:
            self.set_attribute("portee", Portee.COURTE)
        else:
            self.set_attribute("portee", dict_portee[actual_value + value])
