from django.urls import path
from .views import home, personnage, campagne

app_name = "fallout"

urlpatterns = [
    #path('login', login.login, name="Login"),
    path('', home.home, name="Home"),
    path('personnage/', personnage.list_all_personnage, name="list-personnage"),
    path('personnage/<int:pk>', personnage.display_personnage, name="detail-personnage"),
    path('personnage/create', personnage.create_personnage, name="create-personnage"),
    path('personnage/update/<int:pk>', personnage.update_personnage, name="update-personnage"),
    path('quest/', campagne.list_all_campagne, name="list-campagne"),
    path('quest/<int:pk>/', campagne.display_campagne, name="detail-campagne")
]
