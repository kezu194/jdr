from django.forms import ModelForm

from fallout.models.personnage import Personnage


class PersonnageForm(ModelForm):
    class Meta:
        model = Personnage
        fields = [
            "name",
            "origine",
            "force",
            "perception",
            "endurance",
            "charisme",
            "intelligence",
            "agilite",
            "chance",
        ]
