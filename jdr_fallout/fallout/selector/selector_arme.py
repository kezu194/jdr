from typing import Iterable, List
from fallout.models.arme import EffetArme, Qualites, TypeArme, ArmeLootable
from fallout.models.effet_arme import LIST_EFFET_ARME
from fallout.models.qualites import LIST_QUALITE_ARME


def get_arme_effet(name: str) -> EffetArme:
    """
    Return a ArmeEffet Object based on the name given.

    Possible ArmeEffet name:
    - "Brutal"
    - "De zone"
    - "Destructeur"
    - "En rafale"
    - "Étourdissant"
    - "Perforant"
    - "Persistant"
    - "Radioactif"

    :param name: name of the ArmeEffet objects
    :return:
    """
    if name not in LIST_EFFET_ARME:
        raise ValueError(
            f"Selected effet: {name} not in possible choices."
            f"Choices are: {LIST_EFFET_ARME}"
        )
    return EffetArme.objects.get(name=name)


def get_arme_qualite(name: str) -> Qualites:
    """
    Return a Qualites Object based on the name given.

    Possible Qualites name:
    - "Combat rapproché"
    - "Deux mains"
    - "Dissimulé"
    - "Fiable"
    - "Gatling"
    - "Imprécis"
    - "Imprévisible"
    - "Invalidant"
    - "Lancer (C)"
    - "Lancer (M)"
    - "Mine"
    - "Parade"
    - "Précis"
    - "Reco"
    - "Silencieux"
    - "Vision nocturne"
    - "Zone d'impact"

    :param name: name of the Qualites objects
    :return:
    """
    if name not in LIST_QUALITE_ARME:
        raise ValueError(
            f"Selected effet: {name} not in possible choices."
            f"Choices are: {LIST_QUALITE_ARME}"
        )
    return Qualites.objects.get(name=name)


def get_arme_by_type(type: TypeArme) -> Iterable[ArmeLootable]:
    """
    Get the list of all ArmeLootable of the given type.

    :param type: type of the requested armes
    :return: Return the list of all ArmeLootable of the same type
    """
    return ArmeLootable.objects.filter(type=type)


def select_list_of_arme_by_type(type: TypeArme, **kwargs) \
        -> List[ArmeLootable]:
    """
    Select a list of ArmeLootable based on the given type.
    You can specify a list of arme to exclude from the selection with the keyword 'excludes'.
    You could also specify a list of precise ArmeLootable name to include in the selection with the keyword 'include'.

    :raises ValueError: Return an error if one of the given names is not found
    :return: Return a list of all ArmeLootable Objects based on the given names
    """

    if kwargs.get("excludes"):
        list_arme = get_arme_by_type(type=type).exclude(name__in=kwargs["excludes"])
        if len(list_arme) != len(get_arme_by_type(type=type)) - len(kwargs["excludes"]):
            raise Exception(
                f"Error, at least one name given didn't exclude any ArmeLootable ({kwargs.get('excludes')}). "
                f"{len(list_arme)} ArmeLootable found but {len(kwargs.get('excludes'))} name given")

    elif kwargs.get("include"):
        list_arme = ArmeLootable.objects.filter(type=type, name__in=kwargs.get("include"))
        if len(list_arme) != len(kwargs.get("include")):
            raise Exception(f"Error, at least one name given didn't return any ArmeLootable ({kwargs.get('include')}). "
                            f"{len(list_arme)} ArmeLootable found but {len(kwargs.get('include'))} name given")

    else:
        list_arme = ArmeLootable.objects.filter(type=type)

    return list_arme
