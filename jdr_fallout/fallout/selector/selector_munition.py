from fallout.models.munition import Munition


def get_munition_of_owner(name_munition: str, owner_id: int) -> Munition:
    """
    Get a Munition objects of the given name and from the given id.

    :param name_munition: name of the requested Munition type
    :param owner_id: id of the owner of the requested Munition
    :return:
    """
    return Munition.objects.get(proprietaire=owner_id, name=name_munition)
