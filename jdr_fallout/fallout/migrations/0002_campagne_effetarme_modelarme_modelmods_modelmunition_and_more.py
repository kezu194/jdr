# Generated by Django 4.2.1 on 2023-06-17 20:54

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('fallout', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Campagne',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=32)),
                ('xp_point', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='EffetArme',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='ModelArme',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, null=True)),
                ('poid', models.FloatField(null=True, validators=[django.core.validators.MinValueValidator(0)])),
                ('prix', models.IntegerField(null=True, validators=[django.core.validators.MinValueValidator(0)])),
                ('type', models.CharField(choices=[('Lourde', 'Lourde'), ('Énergétique', 'Energetique'), ('Explosif', 'Explosif'), ('Corps à corps', 'Corpsacorps'), ('Légère', 'Legere'), ('Projectile', 'Projectile'), ('Mains nues', 'Mainsnues')], max_length=100)),
                ('degat', models.IntegerField(validators=[django.core.validators.MinValueValidator(1)])),
                ('type_degat', models.CharField(choices=[('Balistique', 'Balistique'), ('Énergétique', 'Energetique'), ('Radiation', 'Radiation'), ('Poison', 'Poison')], max_length=100, null=True)),
                ('perforation', models.IntegerField(null=True, validators=[django.core.validators.MinValueValidator(0)])),
                ('rarete', models.IntegerField(validators=[django.core.validators.MinValueValidator(1)])),
                ('effet', models.ManyToManyField(blank=True, to='fallout.effetarme')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ModelMods',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, null=True)),
                ('poid', models.FloatField(null=True, validators=[django.core.validators.MinValueValidator(0)])),
                ('prix', models.IntegerField(null=True, validators=[django.core.validators.MinValueValidator(0)])),
                ('arme_type', models.CharField(choices=[('Lourde', 'Lourde'), ('Énergétique', 'Energetique'), ('Explosif', 'Explosif'), ('Corps à corps', 'Corpsacorps'), ('Légère', 'Legere'), ('Projectile', 'Projectile'), ('Mains nues', 'Mainsnues')], max_length=100)),
                ('emplacement', models.CharField(choices=[('Culasse', 'Culasse'), ('Canon', 'Canon'), ('Chargeur', 'Chargeur'), ('Poignée', 'Poignee'), ('Crosse', 'Crosse'), ('Viseur', 'Viseur'), ('Bouche', 'Bouche'), ('Condensateur', 'Condensateur'), ('Parabole', 'Parabole'), ('Carburant', 'Carburant'), ('Réservoir à propergol', 'Reservoir Propergol'), ('Buse', 'Buse'), ('Lame', 'Lame'), ('Contondante', 'Contondante'), ('Arme de poing', 'Arme Poing'), ('Autre', 'Autre')], max_length=100)),
                ('compatible_arme', models.ManyToManyField(blank=True, to='fallout.modelarme')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ModelMunition',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, null=True)),
                ('poid', models.FloatField(null=True, validators=[django.core.validators.MinValueValidator(0)])),
                ('prix', models.IntegerField(null=True, validators=[django.core.validators.MinValueValidator(0)])),
                ('rarete', models.IntegerField(validators=[django.core.validators.MinValueValidator(0)])),
                ('quantite_trouve', models.IntegerField(validators=[django.core.validators.MinValueValidator(1)])),
                ('ratio_munition_trouve', models.IntegerField(validators=[django.core.validators.MinValueValidator(1)])),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Qualites',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.AddField(
            model_name='personnage',
            name='bonus_corps_a_corps',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='personnage',
            name='bonus_defense',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='personnage',
            name='chance_point',
            field=models.IntegerField(default=0, validators=[django.core.validators.MinValueValidator(4)]),
        ),
        migrations.AddField(
            model_name='personnage',
            name='charge_maximal',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='personnage',
            name='initiative',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='personnage',
            name='level',
            field=models.IntegerField(default=1),
        ),
        migrations.AddField(
            model_name='personnage',
            name='pv',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='personnage',
            name='pv_max',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='personnage',
            name='resistance_poison',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='personnage',
            name='xp',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='personnage',
            name='agilite',
            field=models.IntegerField(default=6, validators=[django.core.validators.MinValueValidator(4)]),
        ),
        migrations.AlterField(
            model_name='personnage',
            name='chance',
            field=models.IntegerField(default=6, validators=[django.core.validators.MinValueValidator(4)]),
        ),
        migrations.AlterField(
            model_name='personnage',
            name='charisme',
            field=models.IntegerField(default=6, validators=[django.core.validators.MinValueValidator(4)]),
        ),
        migrations.AlterField(
            model_name='personnage',
            name='endurance',
            field=models.IntegerField(default=6, validators=[django.core.validators.MinValueValidator(4)]),
        ),
        migrations.AlterField(
            model_name='personnage',
            name='force',
            field=models.IntegerField(default=6, validators=[django.core.validators.MinValueValidator(4)]),
        ),
        migrations.AlterField(
            model_name='personnage',
            name='intelligence',
            field=models.IntegerField(default=6, validators=[django.core.validators.MinValueValidator(4)]),
        ),
        migrations.AlterField(
            model_name='personnage',
            name='origine',
            field=models.IntegerField(choices=[(1, "Confrérie de l'acier"), (2, 'Goule'), (3, 'Super mutant'), (4, 'Mister handy'), (5, 'Survivant'), (6, "Habitant de l'abri")], null=True),
        ),
        migrations.AlterField(
            model_name='personnage',
            name='perception',
            field=models.IntegerField(default=6, validators=[django.core.validators.MinValueValidator(4)]),
        ),
        migrations.CreateModel(
            name='Arme',
            fields=[
                ('modelarme_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.modelarme')),
            ],
            options={
                'abstract': False,
            },
            bases=('fallout.modelarme',),
        ),
        migrations.CreateModel(
            name='ArmeLootable',
            fields=[
                ('modelarme_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.modelarme')),
            ],
            options={
                'abstract': False,
            },
            bases=('fallout.modelarme',),
        ),
        migrations.CreateModel(
            name='ModsLootable',
            fields=[
                ('modelmods_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.modelmods')),
            ],
            options={
                'abstract': False,
            },
            bases=('fallout.modelmods',),
        ),
        migrations.CreateModel(
            name='MunitionLootable',
            fields=[
                ('modelmunition_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.modelmunition')),
            ],
            options={
                'abstract': False,
            },
            bases=('fallout.modelmunition',),
        ),
        migrations.CreateModel(
            name='Parti',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('position', models.IntegerField(choices=[(1, 'Tete'), (2, 'Buste'), (3, 'Bras Gauche'), (4, 'Bras Droite'), (5, 'Jambe Gauche'), (6, 'Jambe Droite')])),
                ('resistance_balistique', models.IntegerField(default=0)),
                ('resistance_energie', models.IntegerField(default=0)),
                ('resistance_radiation', models.IntegerField(default=0)),
                ('personnage', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='fallout.personnage')),
            ],
        ),
        migrations.AddField(
            model_name='modelarme',
            name='qualite',
            field=models.ManyToManyField(blank=True, to='fallout.qualites'),
        ),
        migrations.CreateModel(
            name='Competence',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(choices=[('Armes à énergie', 'Arme Energie'), ('Armes de corps à corps', 'Arme Corps'), ('Armes légères', 'Arme Legeres'), ('Armes lourdes', 'Armes Lourdes'), ('Athlétisme', 'Athletisme'), ('Crochetage', 'Crochetage'), ('Discours', 'Discours'), ('Discrétion', 'Discretion'), ('Explosifs', 'Explosifs'), ('Mains nues', 'Main Nues'), ('Médecine', 'Medecine'), ('Pilotage', 'Pilotage'), ('Projectiles', 'Projectiles'), ('Réparation', 'Reparation'), ('Sciences', 'Sciences'), ('Survie', 'Survie'), ('Troc', 'Troc'), ('Other', 'Other')], max_length=50)),
                ('attribut', models.CharField(choices=[('FOR', 'Force'), ('PER', 'Perception'), ('END', 'Endurance'), ('CHR', 'Charisme'), ('INT', 'Intelligence'), ('AGI', 'Agilite')], max_length=3, null=True)),
                ('is_atout_perso', models.BooleanField(default=False)),
                ('rang', models.IntegerField(default=0)),
                ('personnage', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='fallout.personnage')),
            ],
        ),
        migrations.AddField(
            model_name='personnage',
            name='quest',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='fallout.campagne'),
        ),
        migrations.CreateModel(
            name='Munition',
            fields=[
                ('modelmunition_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.modelmunition')),
                ('quantite', models.IntegerField(default=0, validators=[django.core.validators.MinValueValidator(0)])),
                ('proprietaire', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='fallout.personnage')),
            ],
            options={
                'abstract': False,
            },
            bases=('fallout.modelmunition',),
        ),
        migrations.CreateModel(
            name='Mods',
            fields=[
                ('modelmods_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.modelmods')),
                ('assigned_to', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='fallout.arme')),
                ('proprietaire', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='fallout.personnage')),
            ],
            options={
                'abstract': False,
            },
            bases=('fallout.modelmods',),
        ),
        migrations.AddField(
            model_name='arme',
            name='proprietaire',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='fallout.personnage'),
        ),
        migrations.CreateModel(
            name='ArmeDistanteLootable',
            fields=[
                ('armelootable_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.armelootable')),
                ('cadence', models.IntegerField(null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(6)])),
                ('portee', models.CharField(choices=[('Courte', 'Courte'), ('Moyenne', 'Moyenne'), ('Longue', 'Longue'), ('Extrême', 'Extreme')], max_length=100, null=True)),
                ('munition_type', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='fallout.modelmunition')),
            ],
            options={
                'abstract': False,
            },
            bases=('fallout.armelootable',),
        ),
        migrations.CreateModel(
            name='ArmeDistante',
            fields=[
                ('arme_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.arme')),
                ('cadence', models.IntegerField(null=True, validators=[django.core.validators.MinValueValidator(0), django.core.validators.MaxValueValidator(6)])),
                ('portee', models.CharField(choices=[('Courte', 'Courte'), ('Moyenne', 'Moyenne'), ('Longue', 'Longue'), ('Extrême', 'Extreme')], max_length=100, null=True)),
                ('munition_type', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='fallout.modelmunition')),
            ],
            options={
                'abstract': False,
            },
            bases=('fallout.arme',),
        ),
    ]
