# Generated by Django 4.2.1 on 2023-07-31 07:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('fallout', '0002_campagne_effetarme_modelarme_modelmods_modelmunition_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='Effet',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.RemoveField(
            model_name='modelmods',
            name='arme_type',
        ),
        migrations.AddField(
            model_name='arme',
            name='based_on',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='fallout.armelootable'),
        ),
        migrations.AddField(
            model_name='armedistante',
            name='nombre_munition_par_tir',
            field=models.IntegerField(default=1),
        ),
        migrations.AddField(
            model_name='armedistantelootable',
            name='nombre_munition_par_tir',
            field=models.IntegerField(default=1),
        ),
        migrations.AddField(
            model_name='modelarme',
            name='bonus_furtivite',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='modelarme',
            name='type_degat_persistance',
            field=models.CharField(choices=[('Balistique', 'Balistique'), ('Énergétique', 'Energetique'), ('Radiation', 'Radiation'), ('Poison', 'Poison')], max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='modelmods',
            name='description_effet',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='armedistante',
            name='munition_type',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='fallout.munition'),
        ),
        migrations.AlterField(
            model_name='modelmods',
            name='compatible_arme',
            field=models.ManyToManyField(blank=True, to='fallout.armelootable'),
        ),
        migrations.CreateModel(
            name='DegatPlus1Energetique',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='Effet3Charges',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='Effet4Charges',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='Effet5Charges',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='Effet6Charges',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetAuto',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetAutomatique',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetBaionnette',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetBouche',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetBrutal',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetBrutalDegatPlus1',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetBrutalEnergetique',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetCadencePlus1',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetCadencePlus1Imprecis',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetCadencePlus1Imprevisible',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetCadencePlus2',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetCadencePlus2PorteeMoins1',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetCalibre50',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetCanonAuto',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetCanonLong',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetCanonScie',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetCanonSniper',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetCartouche308',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetCartouche38',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetCartouche45',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetCharge',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetCombatRapprochePorteePlus1',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetCompensationRecul',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetCrosseCompensateur',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetCrosseComplete',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetDegatPlus1',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetDegatPlus1CadenceMoins1',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetDegatPlus1Persistant',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetDegatPlus1PersistantFurtif',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetDegatPlus1PorteePlus1',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetDegatPlus2',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetDegatPlus2Energetique',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetDegatPlus2Perforant',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetDegatPlus3CadencePlus1',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetDegatPlus3Energetique',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetDentElectrique',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetDeuxMainsImprecisCombatRapproche',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetDispersion',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetElectrique',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetEtourdissant',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetFiable',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetGrandePuissance',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetGrandeVitesse',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetIgnoreAbri',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetImprecis',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetLameCourbe',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetLameFurtive',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetLanceFlamme',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetPerdImprecis',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetPerdImprecisCadencePlus1',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetPerdImprecisPerforantPlus1',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetPerforantCombatRapproche',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetPerforantDegatPlus1',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetPersistanceEnergetique',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetPersistant',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetPorteePlus1',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetPorteePlus1CadencePlus1',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetPorteePlus1CadencePlus1Fiable',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetPrecis',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetPrecisPorteePlus1',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetPrecisReco',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetPrecisVisionNocturne',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetPrecisVisionNocturnePorteePlus1',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetRelancerJetLocalisation',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetScattergun',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetSilencieux',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetTireurElite',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='EffetTireurEliteV2',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='PerforantPlus1',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.CreateModel(
            name='PerforantPorteePlus1',
            fields=[
                ('effet_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='fallout.effet')),
            ],
            bases=('fallout.effet',),
        ),
        migrations.AddField(
            model_name='modelmods',
            name='effet',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='fallout.effet'),
        ),
    ]
