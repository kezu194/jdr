from typing import Any
from django.core.management.base import BaseCommand, CommandParser
from fallout.models import EffetArme, Qualites, MunitionLootable, TypeArme, TypeDegat, Portee, ArmeDistanteLootable, \
    ArmeLootable, Aptitude
from fallout.models.effet import EffetDegatPlus1, EffetDegatPlus2, EffetDegatPlus3CadencePlus1, EffetBrutal, \
    EffetAutomatique, EffetCartouche38, EffetCartouche308, EffetCartouche45, EffetCalibre50, \
    EffetCadencePlus2PorteeMoins1, EffetImprecis, EffetFiable, EffetPorteePlus1, EffetPorteePlus1CadencePlus1, \
    EffetPorteePlus1CadencePlus1Fiable, EffetCanonScie, EffetDegatPlus1PorteePlus1, EffetCadencePlus1Imprevisible, \
    EffetCadencePlus1, EffetPerdImprecis, EffetPerdImprecisPerforantPlus1, EffetCrosseComplete, EffetTireurElite, \
    EffetRelancerJetLocalisation, EffetPrecis, EffetPrecisPorteePlus1, EffetPrecisVisionNocturne, \
    EffetPrecisVisionNocturnePorteePlus1, EffetPrecisReco, EffetBaionnette, EffetPerdImprecisCadencePlus1, \
    EffetSilencieux, Effet3Charges, Effet4Charges, Effet5Charges, Effet6Charges, EffetElectrique, EffetAuto, \
    EffetCrosseCompensateur, \
    EffetPersistant, EffetDegatPlus1CadenceMoins1, EffetBrutalDegatPlus1, EffetCombatRapprochePorteePlus1, \
    EffetScattergun, EffetCanonAuto, EffetCanonLong, EffetCanonSniper, EffetLanceFlamme, \
    EffetDeuxMainsImprecisCombatRapproche, EffetPerforantCombatRapproche, EffetTireurEliteV2, EffetCompensationRecul, \
    EffetDispersion, EffetCadencePlus1Imprecis, EffetCadencePlus2, EffetIgnoreAbri, PerforantPlus1, EffetCharge, \
    PerforantPorteePlus1, EffetGrandeVitesse, EffetGrandePuissance, DegatPlus1Energetique, EffetDentElectrique, \
    EffetEtourdissant, EffetDegatPlus1Persistant, EffetDegatPlus1PersistantFurtif, EffetLameFurtive, EffetLameCourbe, \
    EffetPerforantDegatPlus1, EffetDegatPlus2Perforant, EffetDegatPlus3, EffetDegatPlus2Energetique, \
    EffetDegatPlus3Energetique, EffetDegatPlus1Energetique, EffetPerforant
from fallout.models.mods import EmplacementMods, ModsLootable
from fallout.selector.selector_arme import select_list_of_arme_by_type


def populate_effet():
    list_effets = (
        "Brutal",
        "De zone",
        "Destructeur",
        "En rafale",
        "Étourdissant",
        "Perforant",
        "Persistant",
        "Radioactif"
    )
    for effet in list_effets:
        EffetArme(name=effet).save()


def populate_qualites():
    list_qualites = (
        "Combat rapproché",
        "Deux mains",
        "Dissimulé",
        "Fiable",
        "Gatling",
        "Imprécis",
        "Imprévisible",
        "Invalidant",
        "Lancer (C)",
        "Lancer (M)",
        "Mine",
        "Parade",
        "Précis",
        "Reco",
        "Silencieux",
        "Vision nocturne",
        "Zone d'impact"
    )
    for qualite in list_qualites:
        Qualites(name=qualite).save()


def populate_munition():
    list_munitions = (
        # name, quantite, ratio, poid, cout, rarete
        ("Cartouche .38", 10, 5, 0.1, 1, 0),
        ("Cartouche 10mm", 8, 4, 0.1, 2, 0),
        ("Cartouche .308", 6, 3, 0.1, 3, 1),
        ("Fusée éclairante", 2, 1, 0.1, 1, 1),
        ("Calibre 12", 6, 3, 0.1, 3, 1),
        ("Cartouche .45", 8, 4, 0.1, 3, 2),
        ("Carburant de lance-flammes", 12, 6, 0.1, 1, 2),
        ("Cellule à fusion", 14, 7, 0.1, 3, 2),
        ("Cartouche Gamma", 4, 2, 0.1, 10, 2),
        ("Clou de rail", 6, 3, 0.1, 1, 2),
        ("Cartouche .44", 4, 2, 0.1, 3, 3),
        ("Calibre .50", 4, 2, 0.1, 4, 3),
        ("Cartouche 5.56mm", 8, 4, 0.1, 2, 3),
        ("Cartouche 5mm", 280, 6, 0.05, 1, 3),
        ("Réacteur à fusion", 0, 1, 2, 200, 3),
        ("Missile", 2, 1, 3.5, 25, 3),
        ("Cartouche au plasma", 10, 5, 0.1, 5, 4),
        ("CE 2mm", 6, 3, 0.1, 10, 5),
        ("Mini bombe nucléaire", 1, 1, 6, 100, 6)
    )

    for munition in list_munitions:
        MunitionLootable(name=munition[0], quantite_trouve=munition[1], ratio_munition_trouve=munition[2],
                         poid=munition[3], prix=munition[4], rarete=munition[5]).save()

    # list_seringues = (
    #     ("Artibloc", 40),
    #     ("Dangerol", 60),
    #     ("Embrumaze", 73),
    #     ("Escampoudréine", 55),
    #     ("Folie furieuse", 50),
    #     ("Hémorragie", 17),
    #     ("Larve de mouche bouffie", 10),
    #     ("Pax", 39),
    #     ("Venin de radscorpion", 65)
    # )

    # TODO faire munition type seringue
    # for seringue in list_seringues:
    #     ModelSeringue(name=seringue[0], prix=seringue[1], quantite_trouve=4, ratio_munition_trouve=2, poid=0.1, rarete=2).save()


def populate_arme():
    # Raccourci des effets
    brutal = EffetArme.objects.get(name="Brutal")
    zone = EffetArme.objects.get(name="De zone")
    destructeur = EffetArme.objects.get(name="Destructeur")
    rafale = EffetArme.objects.get(name="En rafale")
    etoudissant = EffetArme.objects.get(name="Étourdissant")
    perforant = EffetArme.objects.get(name="Perforant")
    persistant = EffetArme.objects.get(name="Persistant")
    radioactif = EffetArme.objects.get(name="Radioactif")

    # Raccourci des Qualites
    combat_rapproche = Qualites.objects.get(name="Combat rapproché")
    deux_mains = Qualites.objects.get(name="Deux mains")
    dissimule = Qualites.objects.get(name="Dissimulé")
    fiable = Qualites.objects.get(name="Fiable")
    gatling = Qualites.objects.get(name="Gatling")
    imprecis = Qualites.objects.get(name="Imprécis")
    imprevisible = Qualites.objects.get(name="Imprévisible")
    invalidant = Qualites.objects.get(name="Invalidant")
    lancer_c = Qualites.objects.get(name="Lancer (C)")
    lancer_m = Qualites.objects.get(name="Lancer (M)")
    mine = Qualites.objects.get(name="Mine")
    parade = Qualites.objects.get(name="Parade")
    silencieux = Qualites.objects.get(name="Silencieux")
    zone_impact = Qualites.objects.get(name="Zone d'impact")

    # Raccourci Type munition
    cartouche_38 = MunitionLootable.objects.get(name="Cartouche .38")
    cartouche_10mm = MunitionLootable.objects.get(name="Cartouche 10mm")
    cartouche_308 = MunitionLootable.objects.get(name="Cartouche .308")
    fusee_eclairante = MunitionLootable.objects.get(name="Fusée éclairante")
    calibre_12 = MunitionLootable.objects.get(name="Calibre 12")
    cartouche_45 = MunitionLootable.objects.get(name="Cartouche .45")
    carburant = MunitionLootable.objects.get(name="Carburant de lance-flammes")
    cellule_fusion = MunitionLootable.objects.get(name="Cellule à fusion")
    cartouche_gamma = MunitionLootable.objects.get(name="Cartouche Gamma")
    clou_rail = MunitionLootable.objects.get(name="Clou de rail")
    cartouche_44 = MunitionLootable.objects.get(name="Cartouche .44")
    cartouche_5_56mm = MunitionLootable.objects.get(name="Cartouche 5.56mm")
    cartouche_5mm = MunitionLootable.objects.get(name="Cartouche 5mm")
    missile = MunitionLootable.objects.get(name="Missile")
    cartouche_plasma = MunitionLootable.objects.get(name="Cartouche au plasma")
    ce_2mm = MunitionLootable.objects.get(name="CE 2mm")
    mini_bombe_nucleaire = MunitionLootable.objects.get(name="Mini bombe nucléaire")

    list_arme = (
        # Name, Type, degat, EffetDegat, type, dégat, cadence, portee, qualites, poid, cout, rarete, perf, type_munition

        # Arme légère
        ("Pistolet .44", TypeArme.LEGERE, 6, [brutal], TypeDegat.BALISTIQUE, 1, Portee.COURTE, [combat_rapproche], 2,
         99, 2, 0, cartouche_44),
        ("Pistolet 10mm", TypeArme.LEGERE, 4, [], TypeDegat.BALISTIQUE, 2, Portee.COURTE, [combat_rapproche, fiable], 2,
         50, 1, 0, cartouche_10mm),
        ("Pistolet lance-fusées", TypeArme.LEGERE, 3, [], TypeDegat.BALISTIQUE, 0, Portee.MOYENNE, [fiable], 1, 50, 1,
         0, fusee_eclairante),
        ("Fusil d'assaut", TypeArme.LEGERE, 5, [], TypeDegat.BALISTIQUE, 2, Portee.MOYENNE, [deux_mains], 6.5, 144, 2,
         0, cartouche_5_56mm),
        ("Carabine de combat", TypeArme.LEGERE, 5, [], TypeDegat.BALISTIQUE, 2, Portee.MOYENNE, [deux_mains], 5.5, 117,
         2, 0, cartouche_45),
        ("Fusil de gauss", TypeArme.LEGERE, 10, [perforant], TypeDegat.BALISTIQUE, 1, Portee.LONGUE, [deux_mains], 8,
         228, 4, 1, ce_2mm),
        ("Fusil de chasse", TypeArme.LEGERE, 6, [perforant], TypeDegat.BALISTIQUE, 0, Portee.MOYENNE, [deux_mains], 5,
         55, 2, 1, cartouche_308),
        ("Mitraillette", TypeArme.LEGERE, 3, [rafale], TypeDegat.BALISTIQUE, 3, Portee.COURTE, [deux_mains, imprecis],
         6, 109, 1, 0, cartouche_45),
        ("Fusil de combat", TypeArme.LEGERE, 5, [zone], TypeDegat.BALISTIQUE, 2, Portee.COURTE, [deux_mains, imprecis],
         5.5, 87, 2, 0, calibre_12),
        ("Fusil à double canon", TypeArme.LEGERE, 5, [brutal, zone], TypeDegat.BALISTIQUE, 1, Portee.COURTE,
         [deux_mains, imprecis], 4.5, 39, 1, 0, calibre_12),
        ("Arme à verrou de fortune", TypeArme.LEGERE, 5, [perforant], TypeDegat.BALISTIQUE, 0, Portee.COURTE,
         [imprevisible], 1.5, 30, 0, 1, cartouche_308),
        ("Arme de fortune", TypeArme.LEGERE, 3, [], TypeDegat.BALISTIQUE, 2, Portee.COURTE,
         [combat_rapproche, imprevisible], 1, 30, 0, 0, cartouche_38),
        ("Revolver de fortune", TypeArme.LEGERE, 4, [], TypeDegat.BALISTIQUE, 1, Portee.COURTE,
         [combat_rapproche, imprevisible], 2, 25, 0, 0, cartouche_45),
        ("Fusil à clous", TypeArme.LEGERE, 10, [destructeur], TypeDegat.BALISTIQUE, 0, Portee.MOYENNE,
         [deux_mains, imprevisible, invalidant], 7, 290, 4, 0, clou_rail),

        # Arme laser
        ("Laser de l'institut", TypeArme.ENERGETIQUE, 3, [rafale], TypeDegat.ENERGETIQUE, 3, Portee.COURTE,
         [combat_rapproche, imprecis], 2, 50, 2, 0, cellule_fusion),
        ("Mousquet laser", TypeArme.ENERGETIQUE, 5, [perforant], TypeDegat.ENERGETIQUE, 0, Portee.MOYENNE, [deux_mains],
         6.5, 57, 1, 1, cellule_fusion),
        ("Arme laser", TypeArme.ENERGETIQUE, 4, [perforant], TypeDegat.ENERGETIQUE, 2, Portee.COURTE,
         [combat_rapproche], 2, 69, 2, 1, cellule_fusion),
        ("Arme plasma", TypeArme.ENERGETIQUE, 6, [], TypeDegat.ENERGETIQUE, 1, Portee.COURTE, [combat_rapproche], 2,
         123, 3, 0, cartouche_plasma),
        ("Pistolet gamma", TypeArme.ENERGETIQUE, 3, [etoudissant, perforant], TypeDegat.RADIATION, 1, Portee.MOYENNE,
         [imprecis, zone_impact], 1.5, 156, 5, 1, cartouche_gamma),

        # Arme lourde
        ("Fat man", TypeArme.LOURDE, 21, [brutal, destructeur, radioactif], TypeDegat.BALISTIQUE, 0, Portee.MOYENNE,
         [deux_mains, imprecis, zone_impact], 15.5, 512, 4, 0, mini_bombe_nucleaire),
        ("Incinérateur lourd", TypeArme.LOURDE, 5, [zone, rafale, persistant], TypeDegat.ENERGETIQUE, 3, Portee.MOYENNE,
         [deux_mains, invalidant], 10, 350, 4, 0, carburant),
        # ("Junk jet", TypeArme.LOURDE, 6, [], TypeDegat.BALISTIQUE, 1, Portee.MOYENNE, [deux_mains], 15, 285, 3, 0, ),
        # PLUSIEURS MUNITIONS
        ("Lance-flammes", TypeArme.LOURDE, 3, [zone, rafale, persistant], TypeDegat.ENERGETIQUE, 4, Portee.COURTE,
         [deux_mains, imprecis, invalidant], 8.5, 137, 3, 0, carburant),
        ("Lance-missiles", TypeArme.LOURDE, 11, [], TypeDegat.BALISTIQUE, 0, Portee.LONGUE, [deux_mains, zone_impact],
         10.5, 314, 4, 0, missile),
        ("Laser gatling", TypeArme.LOURDE, 3, [rafale, perforant], TypeDegat.ENERGETIQUE, 6, Portee.MOYENNE,
         [deux_mains, gatling, imprecis], 9.5, 804, 3, 1, cellule_fusion),
        ("Minigun", TypeArme.LOURDE, 3, [zone, rafale], TypeDegat.BALISTIQUE, 5, Portee.MOYENNE,
         [deux_mains, gatling, imprecis], 13.5, 382, 2, 0, cartouche_5mm),
    )

    for arme in list_arme:
        model_arme = ArmeDistanteLootable(name=arme[0], type=arme[1], degat=arme[2], type_degat=arme[4],
                                          cadence=arme[5], portee=arme[6], poid=arme[8], prix=arme[9], rarete=arme[10],
                                          perforation=arme[11], munition_type=arme[12], type_degat_persistance=arme[4])
        model_arme.save()  # this pre save make it possible to assign ManyToMany attributes
        model_arme.effet.set(arme[3])
        model_arme.qualite.set(arme[7])
        model_arme.save()

    list_arme_corps_a_corps = (
        ("Épée", 4, [perforant], TypeDegat.BALISTIQUE, [parade], 1.5, 50, 2, 1),
        ("Couteau de combat", 3, [perforant], TypeDegat.BALISTIQUE, [], 0.5, 25, 1, 1),
        ("Machette", 3, [perforant], TypeDegat.BALISTIQUE, [], 1, 25, 1, 1),
        ("Éventreur", 4, [brutal], TypeDegat.BALISTIQUE, [], 3, 50, 2, 0),
        ("Flambeur", 5, [perforant], TypeDegat.ENERGETIQUE, [parade], 1.5, 200, 3, 1),
        ("Cran d'arrêt", 2, [perforant], TypeDegat.BALISTIQUE, [dissimule], 0.5, 20, 0, 1),
        ("Batte de baseball", 4, [], TypeDegat.BALISTIQUE, [deux_mains], 1.5, 25, 1, 0),
        ("Batte de baseball en aluminium", 5, [], TypeDegat.BALISTIQUE, [deux_mains], 1, 32, 2, 0),
        ("Planche", 4, [], TypeDegat.BALISTIQUE, [deux_mains], 1.5, 20, 0, 0),
        ("Tuyau de plomb", 3, [], TypeDegat.BALISTIQUE, [], 1.5, 15, 0, 0),
        ("Clé serre-tube", 3, [], TypeDegat.BALISTIQUE, [], 1, 30, 1, 0),
        ("Queue de billard", 3, [], TypeDegat.BALISTIQUE, [deux_mains], 0.5, 10, 10, 0),
        ("Rouleau de pâtisserie", 3, [], TypeDegat.BALISTIQUE, [], 0.5, 10, 0, 0),
        ("Matraque", 3, [], TypeDegat.BALISTIQUE, [], 0.5, 10, 0, 0),
        ("Masse", 5, [], TypeDegat.BALISTIQUE, [], 6, 40, 2, 0),
        ("Super masse", 6, [destructeur], TypeDegat.BALISTIQUE, [deux_mains], 10, 180, 3, 0),
        ("Démonte-pneu", 3, [], TypeDegat.BALISTIQUE, [], 1, 25, 1, 0),
        ("Canne", 3, [], TypeDegat.BALISTIQUE, [], 1, 10, 1, 0),
    )

    for arme in list_arme_corps_a_corps:
        model_arme = ArmeLootable(name=arme[0], type=TypeArme.CORPSACORPS, degat=arme[1], type_degat=arme[3],
                                  poid=arme[5], prix=arme[6], rarete=arme[7], perforation=arme[8])
        model_arme.save()
        model_arme.effet.set(arme[2])
        model_arme.qualite.set(arme[4])
        model_arme.save()

    list_arme_mains_nues = (
        ("Gant de boxe", 3, [etoudissant], TypeDegat.BALISTIQUE, [], 0.5, 10, 1, 0),
        ("Gantelet d'écorcheur", 5, [perforant], TypeDegat.BALISTIQUE, [], 5, 75, 3, 1),
        ("Poing américain", 3, [], TypeDegat.BALISTIQUE, [dissimule], 0.5, 10, 1, 0),
        ("Poing assisté", 4, [etoudissant], TypeDegat.BALISTIQUE, [], 2, 100, 2, 0),
    )

    for arme in list_arme_mains_nues:
        model_arme = ArmeLootable(name=arme[0], type=TypeArme.CORPSACORPS, degat=arme[1], type_degat=arme[3],
                                  poid=arme[5], prix=arme[6], rarete=arme[7], perforation=arme[8])
        model_arme.save()
        model_arme.effet.set(arme[2])
        model_arme.qualite.set(arme[4])
        model_arme.save()

    list_arme_projectile = (
        ("Couteau de lancer", 3, [perforant], TypeDegat.BALISTIQUE, [dissimule, lancer_c, silencieux], 0.5, 10, 1, 1),
        ("Tomahawk", 4, [perforant], TypeDegat.BALISTIQUE, [lancer_c, silencieux], 0.5, 15, 2, 1),
        ("Javelot", 4, [perforant], TypeDegat.BALISTIQUE, [lancer_m, silencieux], 2, 10, 1, 1)
    )
    for arme in list_arme_projectile:
        model_arme = ArmeLootable(name=arme[0], type=TypeArme.PROJECTILE, degat=arme[1], type_degat=arme[3],
                                  poid=arme[5], prix=arme[6], rarete=arme[7], perforation=arme[8])
        model_arme.save()
        model_arme.effet.set(arme[2])
        model_arme.qualite.set(arme[4])
        model_arme.save()

    list_arme_explosif = (
        ("Cocktail molotov", 4, [persistant], TypeDegat.ENERGETIQUE, [lancer_m, zone_impact], 0.5, 20, 1),
        ("Grenade à fragmentation", 6, [], TypeDegat.BALISTIQUE, [lancer_m, zone_impact], 0.5, 50, 2),
        ("Grenade à impulsion", 6, [etoudissant], TypeDegat.ENERGETIQUE, [lancer_m, zone_impact], 0.5, 100, 3),
        ("Grenade à plasma", 9, [], TypeDegat.ENERGETIQUE, [lancer_m, zone_impact], 0.5, 135, 3),
        ("Grenade nuka", 9, [brutal, destructeur, radioactif], TypeDegat.ENERGETIQUE, [lancer_m, zone_impact], 0.5,
         100, 4),
        ("Grenade ronde", 5, [], TypeDegat.BALISTIQUE, [lancer_m, zone_impact], 0.5, 40, 1),
        ("Mine à capsules", 6, [], TypeDegat.BALISTIQUE, [mine, zone_impact], 0.5, 75, 2),
        ("Mine à fragmentation", 6, [], TypeDegat.BALISTIQUE, [mine, zone_impact], 0.5, 50, 2),
        ("Mine à impulsion", 9, [], TypeDegat.ENERGETIQUE, [lancer_m, zone_impact], 0.5, 135, 3),
        ("Mine à plasma", 9, [], TypeDegat.ENERGETIQUE, [lancer_m, zone_impact], 0.5, 135, 3),
        ("Mine nucléaire", 9, [brutal, destructeur, radioactif], TypeDegat.ENERGETIQUE, [lancer_m, zone_impact], 0.5,
         100, 4)
    )

    for arme in list_arme_explosif:
        model_arme = ArmeLootable(name=arme[0], type=TypeArme.EXPLOSIF, degat=arme[1], type_degat=arme[3],
                                  poid=arme[5], prix=arme[6], rarete=arme[7], perforation=0)
        model_arme.save()
        model_arme.effet.set(arme[2])
        model_arme.qualite.set(arme[4])
        model_arme.save()


def populate_mods():
    arme_name_with_chargeur = ["Pistolet 10mm", "Fusil d'assaut", "Carabine de combat", "Fusil de chasse",
                               "Mitraillette", "Fusil de combat", "Arme de fortune"]
    arme_without_viseur = ["Pistolet .44", "Pistolet 10mm", "Mitraillette", "Fusil à double canon"]

    list_mods_arme_legere = (
        # Nom, Description, Effet, Emplacement, Poid, Prix, arme not compatible
        # Culasse
        ("Renforcée", "+1 de dégâts", EffetDegatPlus1, EmplacementMods.CULASSE, 0, 20,
         {"excludes": ["Fusil de gauss", "Fusil à clous"]}),
        ("Puissante", "+2 de dégâts", EffetDegatPlus2, EmplacementMods.CULASSE, 0.5, 25,
         {"excludes": ["Fusil de gauss", "Fusil à clous"]}),
        ("Avancée", "+3 de dégâts\n+1 cadence de tir", EffetDegatPlus3CadencePlus1, EmplacementMods.CULASSE, 1, 35, {
            "excludes": ["Fusil de gauss", "Fusil de chasse", "Mitraillette", "Arme à verrou de fortune",
                         "Arme de fortune", "Revolver de fortune", "Fusil à clous"]}),
        ("Calibrée", "Gagne Brutal", EffetBrutal, EmplacementMods.CULASSE, 0, 25,
         {"excludes": ["Pistolet .44", "Fusil de gauss", "Mitraillette", "Fusil à double canon", "Fusil à clous"]}),
        ("Automatique", "-1 de dégâts\n+2 cadence de tir\nGagne En rafale\n Gagne Imprécis", EffetAutomatique,
         EmplacementMods.CULASSE, 0.5, 30, {
             "excludes": ["Pistolet .44", "Fusil de gauss", "Fusil de chasse", "Mitraillette", "Fusil à double canon",
                          "Arme à verrou de fortune", "Revolver de fortune", "Fusil à clous"]}),
        ("Haute sensibilité", "+1 cadence de tir", EffetCadencePlus1, EmplacementMods.CULASSE, 0, 20,
         {"excludes": ["Pistolet .44", "Fusil de gauss", "Fusil de chasse", "Mitraillette", "Arme à verrou de fortune",
                       "Revolver de fortune", "Fusil à clous"]}),
        ("Culasse .38", "Fait passer les dégâts à 4\nFait passer les munitions à cartouche .38", EffetCartouche38,
         EmplacementMods.CULASSE, 1.5, 20, {
             "excludes": ["Pistolet .44", "Pistolet 10mm", "Fusil d'assaut", "Fusil de gauss", "Mitraillette",
                          "Fusil de combat", "Fusil à double canon", "Arme de fortune", "Fusil à clous"]}),
        ("Culasse .308", "Fait passer les dégâts à 7\nFait passer les munitions à cartouche .308", EffetCartouche308,
         EmplacementMods.CULASSE, 2, 40, {
             "excludes": ["Pistolet .44", "Pistolet 10mm", "Fusil d'assaut", "Fusil de gauss", "Fusil de chasse",
                          "Mitraillette", "Fusil de combat", "Fusil à double canon", "Arme à verrou de fortune",
                          "Arme de fortune", "Fusil à clous"]}),
        ("Culasse .45", "Fait passer les dégâts à 4\n+1 cadence de tir\nFait passer les munitions à cartouche .45",
         EffetCartouche45, EmplacementMods.CULASSE, 1, 19, {"include": ["Arme de fortune"]}),
        ("Culasse .50", "Fait passer les dégâts à 8\n Gagne Brutal\nFait passer les munitions à calibre .50",
         EffetCalibre50, EmplacementMods.CULASSE, 2, 30, {"include": ["Fusil de chasse", "Arme à verrou de fortune"]}),
        ("Culasse automatique à piston", "+2 cadence de tir\nRéduit la portée de 1 niveau",
         EffetCadencePlus2PorteeMoins1, EmplacementMods.CULASSE, 1, 75, {"include": ["Fusil à clous"]}),

        # Canon
        ("Canon compact", "Gagne Imprécis", EffetImprecis, EmplacementMods.CANON, -0.5, 0,
         {"include": ["Pistolet .44"]}),
        ("Canon extra-lourd", "Gagne Fiable", EffetFiable, EmplacementMods.CANON, 0, 10, {"include": ["Pistolet .44"]}),
        ("Canon long", "Augmente la portée d'1 niveau", EffetPorteePlus1, EmplacementMods.CANON, 0.5, 20,
         {"excludes": ["Pistolet .44", "Fusil de gauss", "Mitraillette"]}),
        ("Canon à ouverture", "Augmente la portée d'1 niveau\n+1 cadence de tir", EffetPorteePlus1CadencePlus1,
         EmplacementMods.CANON, 0.5, 35,
         {"excludes": ["Pistolet .44", "Fusil de gauss", "Mitraillette", "Fusil à double canon", "Fusil à clous"]}),
        ("Canon ventilé", "Augmente la portée d'1 niveau\n+1 cadence de tir\nGagne Fiable",
         EffetPorteePlus1CadencePlus1Fiable, EmplacementMods.CANON, 0.5, 36,
         {"include": ["Fusil d'assaut", "Carabine de combat", "Fusil de chasse"]}),
        ("Canon scié", "Perd Deux mains\nGagne Combat rapproché", EffetCanonScie, EmplacementMods.CANON, 1, 3,
         {"include": ["Fusil à double canon"]}),
        ("Canon protégé", "+1 de dégâts", EffetDegatPlus1, EmplacementMods.CANON, 0, 37,
         {"include": ["Fusil de gauss"]}),
        ("Canon à ailettes", "+1 de dégâts\nAugmente la portée d'1 niveau", EffetDegatPlus1PorteePlus1,
         EmplacementMods.CANON, 1, 15,
         {"include": ["Arme à verrou de fortune", "Arme de fortune", "Revolver de fortune"]}),

        # Chargeur
        ("Grand chargeur", "+1 cadence de tir\nGagne Imprévisible", EffetCadencePlus1Imprevisible,
         EmplacementMods.CHARGEUR, 0.5, -3, {"include": arme_name_with_chargeur}),
        ("Chargeur à éjection rapide", "Gagne Fiable", EffetFiable, EmplacementMods.CHARGEUR, 0, 8,
         {"include": arme_name_with_chargeur}),
        ("Grand chargeur à éjection rapide", "+1 cadence de tir", EffetCadencePlus1, EmplacementMods.CHARGEUR, 0.5, 23,
         {"include": arme_name_with_chargeur}),

        # Poignée
        ("Poignée confort", "Perd Imprécis", EffetPerdImprecis, EmplacementMods.POIGNEE, 0, 6,
         {"include": ["Pistolet .44", "Pistolet 10mm"]}),
        ("Poignée de tireur d'élite", "Perd Imprécis\nGagne Perforant 1", EffetPerdImprecisPerforantPlus1,
         EmplacementMods.POIGNEE, 0, 10,
         {"include": ["Pistolet 10mm", "Arme à verrou de fortune", "Arme de fortune", "Revolver de fortune"]}),

        # Crosse
        ("Crosse complète", "Gagne Deux mains\nPerd Imprécis", EffetCrosseComplete, EmplacementMods.CROSSE, 0.5, 10,
         {"include": ["Fusil d'assaut", "Carabine de combat", "Fusil de chasse", "Mitraillette", "Fusil de combat",
                      "Fusil à double canon"]}),
        ("Crosse de tieur d'élite", "Gagne Deux mains\nPerd Imprécis\nGagne Précis", EffetTireurElite,
         EmplacementMods.CROSSE, 1, 20, {
             "include": ["Fusil d'assaut", "Carabine de combat", "Fusil de chasse", "Fusil de combat",
                         "Arme à verrou de fortune", "Arme de fortune", "Revolver de fortune"]}),
        (
            "Crosse à compensateur de recul", "Gagne Deux mains\nPerd Imprécis\n+1 cadence de tir",
            EffetCrosseCompensateur,
            EmplacementMods.CROSSE, 1, 3,
            {"include": ["Fusil d'assaut", "Carabine de combat", "Fusil de gauss", "Mitraillette", "Fusil de combat",
                         "Arme à verrou de fortune", "Arme de fortune", "Revolver de fortune", "Fusil à clous"]}),

        # Viseur
        ("Viseur laser", "Vous pouvez relancer le dé de localisation", EffetRelancerJetLocalisation,
         EmplacementMods.VISEUR, 0, 14, {"excludes": []}),
        ("Lunette courte", "Gagne précis", EffetPrecis, EmplacementMods.VISEUR, 0.5, 11,
         {"include": ["Fusil d'assaut", "Carabine de combat", "Fusil de gauss", "Fusil de chasse", "Fusil de combat",
                      "Arme à verrou de fortune", "Arme de fortune", "Revolver de fortune", "Fusil à clous"]}),
        ("Lunette longue", "Gagne Précis\nAugmente la portée d'1 niveau", EffetPrecisPorteePlus1,
         EmplacementMods.VISEUR, 0.5, 29, {"excludes": arme_without_viseur}),
        ("Lunette de vision nocturne courte", "Gagne Précis\nGagne Vision nocturne", EffetPrecisVisionNocturne,
         EmplacementMods.VISEUR, 0.5, 38, {"excludes": arme_without_viseur}),
        ("Lunette de vision nocturne longue", "Gagne Précis\nGagne Vision nocturne\nAugmente la portée d'1 niveau",
         EffetPrecisVisionNocturnePorteePlus1, EmplacementMods.VISEUR, 0.5, 50, {"excludes": arme_without_viseur}),
        ("Lunette de reconnaissance", "Gagne Précis\nGagne Reco", EffetPrecisReco, EmplacementMods.VISEUR, 0.5, 59,
         {"excludes": ["Mitraillette", "Fusil à double canon"]}),

        # Bouche
        ("Baïonnette", "Arme de corps à corps, inflige 4 de dégâts Balistique Perforant 1", EffetBaionnette,
         EmplacementMods.BOUCHE, 1, 10, {
             "include": ["Carabine de combat", "Fusil de chasse", "Fusil de combat", "Arme à verrou de fortune",
                         "Arme de fortune", "Revolver de fortune", "Fusil à clous"]}),
        ("Compensateur", "Perd Imprécis", EffetPerdImprecis, EmplacementMods.BOUCHE, 0.5, 15,
         {"excludes": ["Pistolet .44", "Fusil de gauss", "Fusil de chasse", "Fusil à double canon", "Fusil à clous"]}),
        ("Frein de bouche", "Perd Imprécis\n+1 cadence de tir", EffetPerdImprecisCadencePlus1, EmplacementMods.BOUCHE,
         0.5, 30,
         {"include": ["Mitraillette", "Fusil de combat", "Fusil à double canon", "Arme à verrou de fortune",
                      "Arme de fortune", "Revolver de fortune"]}),
        ("Silencieux", "Gagne Silencieux", EffetSilencieux, EmplacementMods.BOUCHE, 1, 45,
         {"excludes": ["Pistolet .44", "Fusil à double canon", "Fusil à clous"]})
    )

    for mods_arme_legere in list_mods_arme_legere:
        # get_or_create return a tuple: (the object and a bool to specify if it's a creation or not)
        effet = mods_arme_legere[2].objects.get_or_create(name=mods_arme_legere[0])[0]
        mods = ModsLootable(name=mods_arme_legere[0], description_effet=mods_arme_legere[1], effet=effet,
                            emplacement=mods_arme_legere[3], poid=mods_arme_legere[4], prix=mods_arme_legere[5],
                            )
        mods.save()
        # The pistolet lance-fusées doesn't include any mods
        if "excludes" in mods_arme_legere[6]:
            mods_arme_legere[6]["excludes"] + ["Pistolet lance-fusées"]
        mods.compatible_arme.set(select_list_of_arme_by_type(type=TypeArme.LEGERE, **mods_arme_legere[6]))
        mods.save()

    list_mods_mourquet_laser = (
        ("Condensateur à trois charges", "+1 dégâts\nConsomme 3 tirs par attaque", Effet3Charges, 0, 4),
        ("Condensateur à quatre charges", "+2 dégâts\nConsomme 4 tirs par attaque", Effet4Charges, 0.5, 8),
        ("Condensateur à cinq charges", "+3 dégâts\nConsomme 5 tirs par attaque", Effet5Charges, 0.5, 12),
        ("Condensateur à six charges", "+4 dégâts\nConsomme 6 tirs par attaque", Effet6Charges, 1, 16),
    )
    arme_mousquet = ArmeLootable.objects.get(name="Mousquet laser")
    for mods_mousquet in list_mods_mourquet_laser:
        effet = mods_mousquet[2].objects.get_or_create(name=mods_mousquet[0])[0]
        mods = ModsLootable(name=mods_mousquet[0], description_effet=mods_mousquet[1], effet=effet,
                            emplacement=EmplacementMods.CONDENSATEUR, poid=mods_mousquet[3], prix=mods_mousquet[4])
        mods.save()
        mods.compatible_arme.set([arme_mousquet])
        mods.save()

    list_mods_pistolet_gamma = (
        ("Parabole à renfoncement", "+1 degâts\nAugmente la portee d'1 niveau", EffetDegatPlus1PorteePlus1,
         EmplacementMods.PARABOLE, 1, 72),
        ("Antenne de transmission",
         "Fait passer les dégâts à 7\nFait passer le type de dégâts à énergétiques\nGagne Radioactif",
         EffetElectrique, EmplacementMods.BOUCHE, 0, 30),
        ("Répéteur de signal", "+2 cadence de tir\nGagne En rafale\nPerd Zone d'impact", EffetAuto,
         EmplacementMods.BOUCHE, 0, 60)
    )
    pistolet_gamma = ArmeLootable.objects.get(name="Pistolet gamma")
    for mods_pistolet_gamma in list_mods_pistolet_gamma:
        effet = mods_pistolet_gamma[2].objects.get_or_create(name=mods_pistolet_gamma[0])[0]
        mods = ModsLootable(name=mods_pistolet_gamma[0], description_effet=mods_pistolet_gamma[1], effet=effet,
                            emplacement=EmplacementMods.CONDENSATEUR, poid=mods_pistolet_gamma[4],
                            prix=mods_pistolet_gamma[5])
        mods.save()
        mods.compatible_arme.set([pistolet_gamma])

    excludes_condensateur = {"excludes": ["Mousquet laser", "Pistolet gamma"]}
    exclude_pistolet_gamma = {"excludes": ["Pistolet gamma"]}
    exclude_bouche = {"excludes": ["Pistolet gamma", "Arme plasma"]}
    list_mods_arme_laser = (

        # Condensateur
        ("Amplificateur d'onde bêta", "Gagne Persistant", EffetPersistant, EmplacementMods.CONDENSATEUR, 0, 30,
         excludes_condensateur),
        ("Condensateur amélioré", "+1 dégâts\n-1 cadence de tirs", EffetDegatPlus1CadenceMoins1,
         EmplacementMods.CONDENSATEUR, 0, 35, excludes_condensateur),
        ("Stimulateur de photons", "Gagne Brutal", EffetBrutal, EmplacementMods.CONDENSATEUR, 0, 30,
         excludes_condensateur),
        (
            "Agitateur de photons", "+1 dégâts\nGagne Brutal", EffetBrutalDegatPlus1, EmplacementMods.CONDENSATEUR, 0.5,
            35,
            excludes_condensateur),

        # Canon
        ("Canon long", "Perd Combat rapproché\n Augmente la portée d'1 niveau", EffetCombatRapprochePorteePlus1,
         EmplacementMods.CANON,
         1, 20, {"excludes": ["Arme plasma", "Pistolet gamma"]}),
        ("Diviseur", "-1 dégâts\nGagne De zone\nGagne Imprécis", EffetScattergun, EmplacementMods.CANON, 0.5, 31,
         {"include": ["Arme plasma"]}),
        ("Canon automatique", "-1 dégâts\nPerd Combat rapproché\nAugemente la portée d'1 niveau\n+1 cadence de tir",
         EffetCanonAuto, EmplacementMods.CANON, 0.5, 24, excludes_condensateur),
        ("Canon long à fixation",
         "Perd Combat rapproché\nAugmente la portée d'1 niveau\nPermet d'accepter un mod de bouche",
         EffetCanonLong, EmplacementMods.CANON, 1, 25, {"include": ["Mousquet laser"]}),
        ("Canon amélioré", "+1 dégâts", EffetDegatPlus1, EmplacementMods.CANON, 0.5, 26, excludes_condensateur),
        ("Canon de précision", "+2 dégâts\nPerd Combat rapproché\nAugmente la portée d'1 niveau\n-1 cadence de tir",
         EffetCanonSniper,
         EmplacementMods.CANON, 1, 30, {"include": ["Arme laser", "Arme plasma"]}),
        ("Canon lance-flamme",
         "-2 dégâts\n+2 cadence de tir\nGagne De zone\nGagne En rafale\nRéduit la portée d'1 niveau\nGagne Imprécis",
         EffetLanceFlamme, EmplacementMods.CANON, 0.5, 35, {"include": ["Arme plasma"]}),

        # Poignée
        ("Poignée de tireur d'élite", "Perd Imprécis\nGagne Perforant 1", EffetPerdImprecisPerforantPlus1,
         EmplacementMods.POIGNEE,
         0, 10, {"include": ["Arme laser"]}),

        # Crosse
        ("Crosse standart", "Gagne Deux mains\nPerd Imprécis\nPerd Combat rapproché",
         EffetDeuxMainsImprecisCombatRapproche,
         EmplacementMods.CROSSE, 0.5, 10, excludes_condensateur),
        ("Crosse Complète", "Gagne Perforant 1\nPerd Combat rapproché", EffetPerforantCombatRapproche,
         EmplacementMods.CROSSE,
         0.5, 15, {"include": ["Mousquet laser"]}),
        ("Crosse de tireur d'élite", "Gagne Deux mains\nPerd Imprécis\nGagne Précis\nPerd Combat rapproché",
         EffetTireurEliteV2,
         EmplacementMods.CROSSE, 1, 20, {'include': ["Arme laser", "Arme plasma"]}),
        ("Crosse à compensateur à recul", "Gagne Deux mains\nPerd Imprécis\n+1 cadence de tir\nPerd Combat rapproché",
         EffetCompensationRecul, EmplacementMods.CROSSE, 1, 3, {"include": ["Arme laser", "Arme plasma"]}),

        # Viseur
        ("Viseur laser", "Vous pouvez relancer le dé de localisation", EffetRelancerJetLocalisation,
         EmplacementMods.VISEUR,
         0, 14, exclude_pistolet_gamma),
        ("Lunette courte", "Gagne Précis", EffetPrecis, EmplacementMods.VISEUR, 0.5, 11, exclude_pistolet_gamma),
        (
            "Lunette longue", "Gagne Précis\nAugmente la portée d'1 niveau", EffetPrecisPorteePlus1,
            EmplacementMods.VISEUR,
            0.5, 29, exclude_pistolet_gamma),
        ("Lunette de vision nocturne courte", "Gagne Précis\nGagne Vision nocturne", EffetPrecisVisionNocturne,
         EmplacementMods.VISEUR,
         0.5, 38, exclude_pistolet_gamma),
        ("Lunette de vision nocturne longue", "Gagne Précis\nGagne Vision nocturne\nAugmente la portée d'1 niveau",
         EffetPrecisVisionNocturnePorteePlus1,
         EmplacementMods.VISEUR, 0.5, 50, exclude_pistolet_gamma),
        ("Lunette de reconnaissance", "Gagne Précis\nGagne Reco", EffetPrecisReco, EmplacementMods.VISEUR, 0.5, 59,
         exclude_pistolet_gamma),

        # Bouche
        (
            "Diviseur de rayon",
            "-1 dégâts\nGagne De zone\n-1 cadence de tir\nGagne Imprécis\nRéduit la portée d'1 niveau",
            EffetDispersion, EmplacementMods.BOUCHE, 0.5, 15, exclude_bouche),
        (
            "Concentration de faisceau", "Augmente la portée d'1 niveau", EffetPorteePlus1, EmplacementMods.BOUCHE, 0.5,
            20,
            exclude_bouche),
        ("Lentille à gyrocompensation", "+1 cadence de tir\nPerd Imprécis", EffetCadencePlus1Imprecis,
         EmplacementMods.BOUCHE,
         0.5, 25, exclude_bouche)
    )

    for mods_arme_laser in list_mods_arme_laser:
        effet, _ = mods_arme_laser[2].objects.get_or_create(name=mods_arme_laser[0])
        mods = ModsLootable(name=mods_arme_laser[0], description_effet=mods_arme_laser[1], effet=effet,
                            emplacement=mods_arme_laser[3], poid=mods_arme_laser[4], prix=mods_arme_laser[5])
        mods.save()
        mods.compatible_arme.set(select_list_of_arme_by_type(type=TypeArme.ENERGETIQUE, **mods_arme_laser[6]))
        mods.save()

    lance_flammes = "Lance-flammes"
    lance_missiles = "Lance-missiles"
    laser_gatling = "Laser gatling"
    minigun = "Minigun"
    list_mods_arme_lourde = (
        # Lance-flammes
        ("Réservoire à napalm", "+1 dégâts", EffetDegatPlus1, EmplacementMods.CARBURANT, 3.5, 59, lance_flammes),
        ("Canon long", "Perd Imprécis", EffetPerdImprecis, EmplacementMods.CANON, 1, 28, lance_flammes),
        ("Grand réservoire", "+1 cadence de tir", EffetCadencePlus1, EmplacementMods.RESERVOIR_PROPERGOL, 1.5, 28,
         lance_flammes),
        ("Réservoire grand", "+2 cadence de tir", EffetCadencePlus2, EmplacementMods.RESERVOIR_PROPERGOL, 3, 34,
         lance_flammes),
        ("Buse de compression", "+1 dégâts", EffetDegatPlus1, EmplacementMods.BUSE, 0, 22, lance_flammes),
        ("Buse de vaporisation", "+1 dégâts\nGagne Brutal", EffetBrutalDegatPlus1, EmplacementMods.BUSE, 0, 47,
         lance_flammes),

        # Lance-missiles
        ("Triple canon", "+1 cadence", EffetCadencePlus1, EmplacementMods.CANON, 8, 143, lance_missiles),
        ("Quadruple canon", "+2 cadence de tir", EffetCadencePlus2, EmplacementMods.CANON, 10, 218, lance_missiles),
        ("Lunette", "Gagne Précis", EffetPrecis, EmplacementMods.VISEUR, 3, 143, lance_missiles),
        ("Lunette de vision nocturne", "Gagne Précis\nGagne Vision nocturne", EffetPrecisVisionNocturne,
         EmplacementMods.VISEUR,
         3, 248, lance_missiles),
        ("Ordinateur de visée", "Ignore le bonus de cover de la cible", EffetIgnoreAbri, EmplacementMods.VISEUR, 3.5,
         293, lance_missiles),
        ("Baïonnette", "Arme de corps à corps, inflige 4 dégâts Balistique", EffetBaionnette, EmplacementMods.BOUCHE,
         0.5, 30, lance_missiles),
        ("Stabilisateur", "Gagne Perforant 1", PerforantPlus1, EmplacementMods.BOUCHE, 1, 60, lance_missiles),

        # Laser gatling
        ("Stimulateur de photons", "Gagne Brutal", EffetBrutal, EmplacementMods.CONDENSATEUR, 0.5, 19, laser_gatling),
        ("Amplificateur d'ondes Bêta", "Gagne Persistant", EffetPersistant, EmplacementMods.CONDENSATEUR, 0.5, 57,
         laser_gatling),
        ("Condensateur amélioré", "+1 dégâts", EffetDegatPlus1, EmplacementMods.CONDENSATEUR, 0.5, 94, laser_gatling),
        ("Agitateur de photons", "+1 dégâts\nGagne Brutal", EffetBrutalDegatPlus1, EmplacementMods.CONDENSATEUR, 1.5,
         132, laser_gatling),
        ("Canons à chargement", "+4 dégâts\n-3 cadence de tir\nAugmente la portée d'1 niveau", EffetCharge,
         EmplacementMods.CANON, 5, 357, laser_gatling),
        ("Viseur laser", "Perd Imprécis", EffetPerdImprecis, EmplacementMods.VISEUR, 0.5, 169, laser_gatling),
        ("Concentrateur de faisceau", "Gagne Perforant 1\nAugmente la portée d'1 niveau", PerforantPorteePlus1,
         EmplacementMods.BUSE, 0, 22, laser_gatling),

        # Minigun
        ("Canon grande vitesse", "+1 dégâts\n+1 cadence de tir\nRéduit la portée d'1 niveau", EffetGrandeVitesse,
         EmplacementMods.CANON, 2.5, 45, minigun),
        ("Triple canon", "+2 dégâts\n-2 cadence de tir", EffetGrandePuissance, EmplacementMods.CANON, 1.5, 75, minigun),
        ("Viseur d'artilleur", "Perd Imprécis", EffetPerdImprecis, EmplacementMods.VISEUR, 0.5, 68, minigun),
        #  ("Boyeur", "Arme de corps à corps, inflige un nombre de dégâts Balistique égale à la cadence de tir"),
    )

    for mods_arme_lourde in list_mods_arme_lourde:
        effet, _ = mods_arme_lourde[2].objects.get_or_create(name=mods_arme_lourde[0])
        mods = ModsLootable(name=mods_arme_lourde[0], description_effet=mods_arme_lourde[1], effet=effet,
                            emplacement=mods_arme_lourde[3], poid=mods_arme_lourde[4], prix=mods_arme_lourde[5])
        mods.save()
        assigned_arme = {"include": [mods_arme_lourde[6]]}
        mods.compatible_arme.set(select_list_of_arme_by_type(type=TypeArme.LOURDE, **assigned_arme))
        mods.save()

    epee = "Épée"
    couteau = "Couteau de combat"
    machette = "Machette"
    eventreur = "Éventreur"
    flambeur = "Flambeur"
    cran = "Cran d'arrêt"
    batte = ["Batte de baseball", "Batte de baseball en aluminium"]
    planche = "Planche"
    tuyau = "Tuyau de plomb"
    serre = "Clé serre-tube"
    queue = "Queue de billard"
    rouleau = "Rouleau de pâtisserie"
    matraque = "Matraque"
    masse = "Masse"
    super_masse = "Super masse"
    pneu = "Démonte-pneu"
    canne = "Canne"
    gant = "Gant de boxe"
    gantelet = "Gantelet d'écorcheur"
    poing = "Poing américain"
    poing_assiste = "Poing assisté"
    list_mods_corps_a_corp = (
        # Épée
        ("Lame dentelée", "Gagne Persistant", EffetPersistant, 0, 25, epee),
        ("Lame électrifiée", "+1 dégâts\nFait passer les types de dégâts à Energetique", DegatPlus1Energetique, 0, 50,
         epee),
        ("Lame dentelée électrifiée",
         "+1 dégâts\nFait passer les types de dégâts à Energetique\nGagne Persistant (Balistique)", EffetDentElectrique,
         0, 75, epee),
        ("Module d'étourdissement", "+2 dégâts\nFait passer les types de dégâts à Energetique\nGagne Étourdissant",
         EffetEtourdissant, 0, 100, epee),

        # Couteau de combat
        ("Lame dentelée", "+1 dégâts\nGagne Persistant", EffetDegatPlus1Persistant, 0, 12, couteau),
        ("Lame furtive", "+1 dégâts\nGagne Persistant\n+2 dégâts sur les attaques furtives",
         EffetDegatPlus1PersistantFurtif, 0, 18, couteau),

        # Machette
        ("Lame dentelée", "+2 dégâts\nGagne Persistant", EffetLameFurtive, 0, 12, machette),

        # Eventreur
        (
        "Lame courbe", "+1 dégâts\nDépensez 2 PA après une attaque réussie pour désarmer l'adversaire", EffetLameCourbe,
        0.5, 15, eventreur),
        ("Lame rallongée", "+1 dégâts\nGagne Persistant", EffetDegatPlus1Persistant, 1.5, 25, eventreur),

        # Flambeur
        ("Jets de flammes supplémentaires", "+1 dégâts\nGagne Persistant", EffetDegatPlus1Persistant, 0.5, 100,
         flambeur),

        # Cran d'arrêt
        ("Lame dentelée", "+1 dégâts\nGagne Persistant", EffetDegatPlus1Persistant, 0, 10, cran),

        # Batte de baseball
        ("Barbelé", "Gagne Perforant 1", PerforantPlus1, 0, 5, batte),
        ("À pointe", "+1 dégâts\nGagne Perforant 1", EffetPerforantDegatPlus1, 0.5, 7, batte),
        ("Affûté", "+1 dégâts\nGagne Persistant", EffetDegatPlus1Persistant, 0.5, 7, batte),
        ("À chaine", "+2 dégâts", EffetDegatPlus2, 0.5, 10, batte),
        ("À lames", "+2 dégâts\nGagne Persistant", EffetLameFurtive, 1, 12, batte),

        # Planche
        ("À pointes", "+1 dégâts\nGagne Perforant 1", EffetPerforantDegatPlus1, 0.5, 6, planche),
        ("Perforant", "+2 dégâts", EffetDegatPlus2, 0.5, 9, planche),
        ("À lames", "+2 dégâts\nGagne Persistant", EffetLameFurtive, 1, 10, planche),

        # Tuyau de plomb
        ("À pointes", "+1 dégâts\nGagne Perforant 1", EffetPerforantDegatPlus1, 0.5, 4, tuyau),
        ("Lourd", "+2 dégâts", EffetDegatPlus2, 1, 11, tuyau),

        # Clé serre-tube
        ("À crochets", "+1 dégâts\nDépensez 2 PA après une attaque réussie pour désarmer l'adversaire", EffetLameCourbe,
         0, 9, serre),
        ("Lourd", "+2 dégâts", EffetDegatPlus2, 3.5, 12, serre),
        ("Perforant", "+2 dégâts\nGagne Perforant 1", EffetDegatPlus2Perforant, 0.5, 13, serre),
        ("Extralourd", "+3 dégâts", EffetDegatPlus3, 1, 22, serre),

        # Queue de billard
        ("Barbelé", "+1 dégâts\nGagne Perforant 1", EffetPerforantDegatPlus1, 0, 2, queue),
        ("Affûté", "+1 dégâts\nGagne Persistant", EffetDegatPlus1Persistant, 0, 3, queue),

        # Rouleau de pâtisserie
        ("À pointes", "+1 dégâts\nGagne Perforant 1", EffetPerforantDegatPlus1, 0, 3, rouleau),
        ("Affûté", "+1 dégâts\nGagne Persistant", EffetDegatPlus1Persistant, 0, 3, rouleau),

        # Matraque
        ("Électrifié", "+2 dégâts\nFait passer le type de dégâts à Énergétiques", EffetDegatPlus2Energetique, 0, 15,
         matraque),
        ("Module d'étourdissement", "+3 dégâts\nFait passer le type de dégâts à Énergétiques",
         EffetDegatPlus3Energetique, 0, 30, matraque),

        # Masse
        ("Perforant", "+1 dégâts\nGagne Perforant 1", EffetPerforantDegatPlus1, 2.5, 18, masse),
        ("Lourd", "+2 dégâts", EffetDegatPlus2, 4.5, 30, masse),

        # Super masse
        ("Bobine thermique", "+1 dégâts\nFait passer le type de dégâts à Énergétiques", EffetDegatPlus1Energetique, 0,
         180, super_masse),
        ("Module d'étourdissement", "+2 dégâts\nGagne Étourdissement\nFait passer le type de dégâts à Énergétiques",
         EffetEtourdissant, 0, 360, super_masse),

        # Démonte pneu
        ("À lames", "+1 dégâts\nGagne Persistant", EffetDegatPlus1Persistant, 0.5, 12, pneu),

        # Canne
        ("Barbelé", "+1 dégâts\nGagne Perforant 1", EffetPerforantDegatPlus1, 0, 3, canne),
        ("À pointes", "+1 dégâts\nGagne Perforant 1", EffetPerforantDegatPlus1, 0, 3, canne),

        # Gant de boxe
        ("À pointes", "Gagne Perforation 1", EffetPerforant, 0, 3, gant),
        ("Perforant", "+1 dégâts\nGagner Perforant 1", EffetPerforantDegatPlus1, 0, 4, gant),
        ("Revêtement en plomb", "+2 dégâts", EffetDegatPlus2, 0.5, 7, gant),

        # Gantelet d'écorcheur
        ("Griffe supplémentaire", "+1 dégâts\nDépensez 2 PA après une attaque réussie pour désarmer l'adversaire",
         EffetLameCourbe, 1, 22, gantelet),

        # Poing américain
        ("Affûté", "Gagne Persistant", EffetPersistant, 0, 3, poing),
        ("À pointes", "Gagne Perforant 1", EffetPerforant, 0, 3, poing),
        ("Perforant", "+1 dégâts\nGagne Perforant 1", EffetPerforantDegatPlus1, 0, 4, poing),
        ("À lames", "+1 dégâts\nGagne Persistant", EffetDegatPlus1Persistant, 0, 5, poing),

        # Poing assisté
        ("Perforant", "+2 dégâts\nGagne Perforant 1", EffetDegatPlus2Perforant, 0.5, 45, poing_assiste),
        ("Bobine thermique", "+2 dégâts\nFait passer le type de dégâts à Énergétiques", EffetDegatPlus2Energetique, 0,
         100, poing_assiste),
    )

    for mods_corps_a_corp in list_mods_corps_a_corp:
        effet, _ = mods_corps_a_corp[2].objects.get_or_create(name=mods_corps_a_corp[0])
        mods = ModsLootable(name=mods_corps_a_corp[0], description_effet=mods_corps_a_corp[1], effet=effet,
                            emplacement=EmplacementMods.LAME, poid=mods_corps_a_corp[3], prix=mods_corps_a_corp[4])
        mods.save()
        if isinstance(mods_corps_a_corp[5], list):
            assigned_arme = {"include": mods_corps_a_corp[5]}
        else:
            assigned_arme = {"include": [mods_corps_a_corp[5]]}
        mods.compatible_arme.set(select_list_of_arme_by_type(type=TypeArme.CORPSACORPS, **assigned_arme))
        mods.save()


def populate():
    populate_effet()
    populate_qualites()
    populate_munition()
    populate_arme()
    populate_mods()
    populate_aptitude()


def populate_aptitude():
    # prerequis format: {"LVL": [1,4,7], "SPECIAL": {"FOR": 6, "END": 7}}

    list_aptitude = (
        ("Ami des animaux", [1, 6], {"CHR": 6},
         "Au rang 1, chaque fois qu'un PNJ créature dont le profil affiche le mot-clef Mammifère, Lézard ou Insecte souhaite vous attaquer, jetez 1 D6: si vous obtenez un résultat autre qu'un Effet, la créature choisit de ne pas vous attaquer, bien qu'elle puisse toujours s'en prendre à un autre personnage\n\nAu rang 2, vous pouvez entreprendre un test de CHR + Survie de difficulté 2 au prix d'une action capitale. En cas de réussite, l'animal vous considère comme amical et attaquera quiconque s'en prend à vous. Cette aptitude ne fonctionne pas sur les animaux redoutables ou légendaires"),
        ("Amphibie", [1, 4], {"END": 5},
         "L'eau est votre alliée. Au rang 1, vous ne subissez plus de dégâts de radiation lorsque vous nagez dans une eau irradiée et vous pouvez retenir votre souffle deux fois plus longtemps.\n\nAu rang 2, les ennemis ajoutent +2 à la difficulté de leurs tests pour vous repérer lorsque vous êtes complètement immergé"),
        ("Antiradiation", [1, 5], {"END": 8},
         "Votre résistance aux dégâts de radiation, quelle que soit la localisation, augmente de +1 par rang dans cette aptitude"),
        ("Armurier", [1, 5, 9, 13], {"FOR": 5, "INT": 6},
         "Vous pouvez modifier les pièces d'armure grâce à des mods d'amure. Chaque rang dans cette aptitude débloque un rang de mod : le rang 1 débloque les mods de rang 1, le rang 2 débloque les mods de rang 2, etc.."),
        ("Barbare", [4], {"FOR": 7},
         "Votre force influe sur vos résistances aux dégâts des différentes localisations. Vous ne tirez aucun bénéfice de cette aptitude lorsque vous portez une armure assistée.\n\nFOR 7-8 : RD balistique +1\nFOR 9-10: RD balistique +2\nFOR 11+ : RD balistique +3"),
        ("Barycentre", [1], {"AGI": 7},
         "Lorsque vous effectuer une attaque à distance, vous pouvez toujours choisir de tirer dans le buste de la cible, le tout, sans augmenter la difficulté de votre attaque. De plus, vous pouvez relancer 1D20 du résultat de votre attaque"),
        ("Beau parleur", [1], {"CHR": 6},
         "Vous pouvez relancer 1D20 lors de tout test de Troc ou de Discours en opposition"),
        ("Blitz", [1, 4], {"AGI": 9},
         "Lorsque vous vous placez à portée de main d'un adversaire et effectuer une attaque au corps à corps contre lui, vous pouvez relancer 1D20 du résultat de votre attaque. De plus au rang 2, cette attaque inflige +1 D6 de dégâts"),
        ("Boyaux plombés", [1, 5], {"END": 6},
         "Au rang 1, vous pouvez relancer le D6 qui détermine si vous subissez des dégâts de radiation en consommant de la nourriture ou une boisson irradiée.\n\nAu rang 2, vous êtes immunisé aux dégâts de radiation infligés par la consommation de nourriture ou de boisson irradiée"),
        ("Bricolage", [1], {},
         "Vous pouvez réparer un objet sans avoir besoin des pièces détachées. Néanmoins cette réparation est temporaire : L'objet se brisera à nouveau lors de la prochaine complication que vous obtiendrez en l'utilisant. De plus, la marge de complication de tout test de compétence effectué à l'aide de cet objet augmente de 1"),
        ("Canaille", [1], {"CHR": 7},
         "Vous pouvez ignorer la première complication obtenue sur tout test de CHR + Discours destiné à convaincre quelqu'un d'un mensonge"),
        # ("Canigou"),
        ("Chasseur", [1], {"END": 6},
         "Les attaques que vous effectuer contre les personnages dont le profil affiche le mot-clef Mammifère, Lésard ou Insecte ainsi que le mot-clef Mutant, obtiennent l'effet de dégâts Brutal, si elles n'en bénéficie pas déjà"),
        ("Chimie", [1], {"INT": 7},
         "La durée d'effet des droques que vous créez est doublée. De plus vous débloquez les recettes qui nécéssitent de posséder cette aptitude"),
        ("Chirurgien de la chevrotine", [1], {"FOR": 5, "AGI": 7},
         "Les attaques que vous effectuer avec un fusil à pompe obtiennent l'effet de dégâts Perforant 1. Si elles bénéficient déjà de cet effet, la valeur de ce dernier augmente de 1"),
        ("Cible mouvante", [1], {"AGI": 6},
         "Lorsque vous sprintez, augmentez de +1 votre défense jusqu'au début de votre prochain tour"),
        ("Cogneur", [1], {"FOR": 6},
         "Lorsque vous effectuer une attaque de corps à corps en cognant votre arme à feu, votre arme obtient l'effet Brutal"),
        ("Commandant laser", [2, 6], {"PER": 8},
         "Lorsque vous effectuer une attaque avec une arme à énergie à distance, ajoutez +1 D6 aux dégâts de l'arme par rang dans cette aptitude"),
        ("Commando", [2, 5], {"AGI": 8},
         "Lorsque vous effectuez une attaque à distance avec une arme dotée d'une cadence de tir supérieure ou égale à 3 (à l'exception des armes lourdes), ajoutez +1 D6 aux dégâts de l'arme par rang dans cette aptitude"),
        ("Compréhension", [1], {"INT": 6},
         "Après avoir utilisé le bonus conféré par la lecture d'un magazine, jetez 1 D6. Si vous obtenez un Effet, vous pouvez utiliser ce bonus une fois de plus"),
        ("Concerto de converses", [1], {"CHA": 5},
         "Lorsque vous fouillez un lieu contenant de la nourriture, vous découvrez un aliment supplémentaire, tiré au hasard, et ce, sans dépenser de PA"),
        ("Coups super critiques", [1], {"CHA": 9},
         "Lorsque vous infligez au moins 1 point dégâts à un ennemi, vous pouvez dépenser 1 point de chance pour lui faire subir automatiquement un coup critique et ainsi causer une blessure"),
        ("Dégainage rapide", [1], {"AGI": 6},
         "Chaque tour, vous pouvez dégainer une seule arme ou un seul objet que vous transportez sans y consacrer une action mineure"),
        ("Dénicheur de trésors", [2, 6, 10], {"CHA": 5},
         "Chaque fois que vous lancer les dés pour savoir combien d'argent vous trouvez, vous en trouvez plus. Au rang 1, vous trouvez +3 D6 supplémentaires de capsules. Au rang 2, +6 D6 supplémentaires. Au rang 3, +10 D6 supplémentaires"),
        ("Énergie solaire", [1], {"END": 7},
         "Vous éliminez 1 point de dégâts de radiation par heure passée à la lumière directe du soleil"),
        ("Entomologiste", [1], {"INT": 7},
         "Les attaques que vous effectuer contre les personnages dont le profil affiche le mot-clef Insecte obtiennent l'effet de dégâts Perforant 1. Si elles bénéficient déjà de cet effet, la valeur de ce dernier augmente de 1"),
        ("Exercice", [2, 4], {},
         "Augmenter l'un de vos attributs S.P.E.C.I.A.L de 1 rang. Les attributs ne peuvent dépasser 10 avec cette méthode"),
        ("Expert en démolition", [1], {"PER": 6, "CHA": 6},
         "Lorsque vous effectuer une attaque à l'aide d'une arme dotée de la qualité Zone d'impact, l'attaque obtient l'effet de dégâts Brutal. De plus vous débloquez les recettes d'explosifs qui nécéssitent de posséder cette aptitude"),
        ("Expert en robotique", [2, 6, 10], {"INT": 8},
         "Au rang 1, vous pouvez modifier l'armure des robots, leurs armes montées et leurs modules avec des mods de rang 1.\n\nAu rang 2, vous débloquez l'accès aux mods de rang 2 et vous réduisez la difficulté de tous vos tests de réparation de robots.\n\nAu rang 3, vous débloquez l'accès aux mods de rang 3 et vous pouvez reprogrammer les robots pour qu'ils remplissent une tout autre fonction ou altérer leur comportement si le MJ vous y autorise"),
        ("Fana d'armes", [2, 6, 10, 14], {"INT": 6},
         "Vous pouvez modifier les armes légères grâce à des mods d'arme. Chaque rang dans cette aptitude débloque un rang de mod. Le rang 1 débloque les mods de rang 1, le rang 2 débloque les mods de rang 2"),
        ("Fana de capsule", [1], {"CHR": 5},
         "Lorsque vous vendez ou achetez des objets, vous pouvez augmenter ou diminuer le prix des biens échangés de 10%"),
        ("Fantôme", [1], {"PER": 5, "AGI": 6},
         "Lorsque vous entreprenez un test d'AGI + Discrétion alors que vous vous trouvez dans l'ombre ou l'obscurité, le premier D20 supplémentaire est gratuit. La limite de 5D20 s'applique toujours à la réserve"),
        ("Farfouilleur", [1, 6, 11], {"CHA": 6},
         "Chaque fois que vous lancez les dés pour savoir combien de munitions vous trouvez, vous en trouvez plus. Au rang 1, vous trouverez +3 D6 supplémentaires de munitions. Au rang 2, +6 D6 supplémentaires. Au rang 3, +10 D6 supplémentaires."),
        ("Farmer la pharma", [1], {"CHA": 6},
         "Lorsque vous fouillez un lieu contenant des médicaments ou des drogues, vous découvrez un objet supplémentaire, tiré au hasard, et ce, sans dépenser de PA"),
        ("Fêtard", [1], {"END": 6, "CHR": 7},
         "Vous ne pouvez pas devenir dépendant aux boissons alcoolisées, chaque fois que vous buvez ce type de boisson, vous regagnez +2 PV"),
        ("Finesse", [1], {"AGI": 9},
         "Une fois par rencontre de combat, vous pouvez relancer l'intégralité des D6 d'un unique jet de dégâts sans dépenser de point de chance"),
        ("Force de frappe", [1], {"FOR": 8},
         "Lorsque vous efffectuer une attaque au corps à corps avec une arme de corps à corps à deux mains, votre attaque obtient l'effet de dégâts Brutal"),
        ("Forgeron", [2, 6, 10], {"FOR": 6},
         "Vous pouvez modifier les armes de corps à corps grâce à des mods d'arme. Chaque rang dans cette aptitude débloque un rang de mod: le rang 1 débloque les mods de rang 1, le rang 2 débloque les mods de rang 2, etc.."),
        ("Frappe perforante", [1], {"FOR": 7},
         "Les attaques que vous effectuez à mains nues ou avec une arme de corps à corps à lame obtiennent l'effet de dégâts Perforant 1. Si elles bénéficient déjà de cet effet, la valeur de ce dernier augmente de 1"),
        ("Fusilier", [2, 6], {"AGI": 7},
         "Lorsque vous effectuez une attaque avec une arme à distance à deux mains dotée d'une cadence de tir inférieur ou égale à 2 (à l'exception des armes lourdes), ajoutez +1 D6 aux dégâts de l'arme par rang dans cette aptitude.\n\nDe plus au rang 2, ajoutez l'effet de dégâts Perforant 1. Si l'attaque bénéficie déjà de cet effet, la valeur de ce dernier augmente de 1"),
        ("Fusion", [1], {"PER": 10},
         "Lorsque vous tuez un ennemi avec une arme à énergie, il explose. Jetez un nombre de D6 égal à la moitié de la valeur de dégâts de l'arme (arrondi à l'inférieur). Pour chaque Effet obtenu, une créature située à portée courte ou inférieur de votre victime subit un nombre de points de dégâts énergétiques égal au total obtenu sur les D6"),
        ("Guérison rapide", [1], {"END": 6},
         "Lorsque vous effectuez un test d'END + Survie pour traiter vos propres blessures, le premier D20 supplémentaire est gratuit. La limite de 5D20 s'applique toujours à la réserve"),
        ("Guérisseur", [1, 6, 11], {"INT": 7},
         "Lorsque vous soignez les PV d'un patient par l'action mineure Porter secours, soignez 1 PV supplémentaire par rang dans cette aptitude"),
        ("Ho-hisse", [1], {"FOR": 8},
         "Lorsque vous effectuer une attaque à distance avec un projectile, vous pouvez dépenser 1 PA pour augmenter la portée de l'arme d'un niveau"),
        ("Homme d'action", [1], {},
         "Lorsque vous dépensez des PA pour obtenir une action capitale supplémentaire, vous ne subissez pas d'augmentation de difficulté pour le test de compétence lié à cette action"),
        ("Infiltrateur", [1], {"PER": 8},
         "Vous pouvez relancer 1D20 lors de tout test de Crochetage destiné à déverrouiller une porte ou un conteneur"),
        ("Infirmier", [1], {"INT": 8},
         "Lorsque vous utilisez l'action mineure Portez secours pour essayer de soigner une blessure, vous pouvez relancer 1D20"),
        ("La taille compte", [1, 5, 9], {"END": 7, "AGI": 6},
         "Lorsque vous effectuez une attaque à distance avec une arme lourde, ajoutez +1 D6 aux dégâts de l'arme par rang dans cette aptitude"),
        ("Locomotive", [1, 6], {"FOR": 9, "END": 7},
         "Au prix d'une action capitale, vous pouvez charger si vous portez une armure assitée ou êtes un super mutant. Vous ne pouvez pas vous déplacer ou sprintez au cours du même tour. Vous vous placez à portée de main d'un ennemi situé à portée moyenne ou inférieure, puis effectuer un test de FOR + Athlétisme de difficulté 2. En cas de réussite, votre ennemi subit vos dégâts à mains nues normaux et vous le renversez"),
        ("Mains lestes", [1], {"AGI": 8},
         "Vous pouvez recharger plus rapidement vos armes à feu. Lorsque vous effectuer une attaque à distance, vous pouvez dépenser 2 PA pour augmenter de +2 la cadence de tir de votre arme ppur cette attaque uniquement"),
        ("Maître-voleur", [1], {"PER": 8, "AGI": 9},
         "Lorsque vous entreprenez un test pour crocheter une serrure ou faire les poches de quelqu'un, la difficulté du test de votre adversaire pour vous repérer augmente de +1"),
        ("Marchand de sable", [1], {"AGI": 9},
         "Lorsque vous effectuez une attaque furtive avec une arme équipée d'un silencieux, ajoutez +2 D6 aux dégâts de l'arme. Vous ne tirez aucun bénéfice de cette aptitude si vous portez une armure assistée"),
        ("Métabolisme rapide", [1, 4, 7], {"END": 6},
         "Lorsque vous récupérez des PV par un autre moyen que le repos, vous récupérez 1 PV supplémentaire par rang dans cette aptitude"),
        ("Mystérieux étranger", [1], {"CHA": 7},
         "Au début d'une rencontre de combat, vous pouvez dépenser 1 point de chance. Si vous le faîtes, le MJ peut faire apparaître, à n'importe quel moment de la scène, le Mystérieux étranger : il effectue une unique attaque à distance contre un adversaire que vous avez déjà attaqué ou qui vient de vous attaquer, puis disparaît. Le MJ doit vous restituer 1 point de chance si le mystérieux étranger n'est pas apparu"),
        ("Nature audacieuse", [1], {"CHA": 7},
         "Lorsque vous entreprenez un test de compétence et achetez au moins 1D20 en octrayant des PA au MJ, vous pouvez relancer 1D20 du résultat. Vous ne pouvez pas sélectionner cette aptitude si vous disposer de l'aptitude 'Nature Prudente'"),
        ("Nature Prudente", [1], {"PER": 7},
         "Lorsque vous entreprenez un test de compétence et achetez au moins 1D20 avec des PA, vous pouvez relancer 1D20 du résultat. Vous ne pouvez pas sélectionner cette aptitude si vous disposer de l'aptitude 'Nature audacieuse'"),
        ("Ninja", [1], {"AGI": 8},
         "Lorsque vous effectuez une attaque furtive à mains nues ou avec une arme de corps à corps, ajoutez +2 D6 aux dégâts de l'attaque. Vous ne tirez aucun bénéfice de cette aptitude si vous portez une armure assistée"),
        ("Nyctalope", [1], {"PER": 7}, "Réduisez de 1 l'augmentation de difficulté due à l'obscurité"),
        ("Paume paralysante", [1], {"FOR": 8},
         "Lorsque vous effectuez une attaque à mains nues et choisissez de cibler une localisation particulière, votre attaque obtient l'effet de dégâts Étourdissant"),
        ("Physicien nucléaire", [1], {"INT": 9},
         "Chaque fois que vous utilisez une arme qui inflige des dégâts de radiation où est dotée de l'effet de dégâts Radioactif, chaque Effet que vous obtenez inflige 1 point de dégâts de radiation supplémentaire"),
        ("Pickpocket", [1, 4, 7], {"PER": 8, "AGI": 8},
         "Au rang 1, vous pouvez ignorer la première complication obtenue lors de tout test d'AGI + Discrétion destiné à dérober un objet que porte une cible sur elle.\n\nAu rang 2, vous pouvez relancer 1D20 lorsque vous essayez de faire les poches de quelqu'un\n\nAu rang 3, réduisez de 1 la difficulté de tout test destiné à faire les poches de quelqu'un"),
        ("Pied léger", [1], {},
         "Lorsque vous obtenez un résultat qui génère une complication sur vos tests de compétence basés sur l'AGI, vous pouvez ignorer une complication par PA dépensé. De plus, vous pouvez relancer 1D20 lors de tout test d'AGI + Athlétisme destiné à éviter les pièges déclenchés par plaque de pression ou tout autre mécanisme similaire"),
        ("Pirate", [1], {"INT": 8}, "Réduisez de 1 la difficulté de vos tests pour pirater les ordinateurs"),
        ("Pisteur", [1], {"PER": 6},
         "Lorsque vous voyagez sur de longues distances dans la nature, un test réussi de PER + Survie (Difficulté déterminé en fonction du terrain) vous permet de diviser par deux le temps de trajet"),
        ("Pistolero", [2, 6], {"AGI": 7},
         "Lorsque vous effectuez une attaque avec une arme à distance à une main dotée d'une cadence de tir inférieur ou égale à 2, ajoutez +1 D6 aux dégâts de l'arme par rang dans cette aptitude. De plus, vous pouvez relancer le dé de localisation des dégâts"),
        ("Poing de fer", [1, 6], {"FOR": 6},
         "Au rang 1, vos attaques à mains nues infligent +1 D6 de dégâts. Au rang 2, elles obtiennent en plus l'effet de dégâts Brutal"),
        ("Poussée d'adrénaline", [1], {"FOR": 7},
         "Lorsque vos points de vie ne sont pas à leur maximum, considérez que votre Force est égale à 10 quand vous entreprenez un test qui l'implique ou effectuer une attaque au corps à corps"),
        ("Présence terrifiante", [3, 8], {"FOR": 6, "CHR": 8},
         "Vous pouvez relancer 1D20 lors de tout test de Discours destiné à menacer ou effrayer quelqu'un.\n\nAu rang 2, en plein combat, vous pouvez consacrer une action capitale à tenter de menacer un ennemi situé à portée moyenne ou inférieure, grâce à un test de FOR + Discours de difficulté 2. En cas de réussite, cet ennemi doit s'éloigner de vous au cours de son prochain tour"),
        ("Pyromane", [2, 6, 10], {"END": 6},
         "Lorsque vous effectuez une attaque avec une arme basée sur le feu, ajoutez +1 D6 aux dégâts de l'arme par rang dans cette aptitude."),
        ("Rage de nerd !", [2, 7, 12], {"INT": 8},
         "Lorsque vous disposez de moins de 1/4 de vos points de PV max, ajoutez +1 à votre RD balistiques, +1 à votre RD Énergétique et +1 D6 aux dégâts de toutes vos attaques.\n\nAu rang 2, ces bonus sont portés à +2 RD et +2 D6.\n\nAu rang 3, ils sont portés à +3 RD et +3 D6 "),
        ("Ragoût de serpent", [1], {"END": 7}, "Votre résistance aux dégâts de poison augmente de 2"),
        ("Recycleur", [3, 8], {},
         "Lorsque vous recycler un objet, vous pouvez obtenir des composants peu fréquents au même titre que des composants fréquents.\n\nAu rang 2, vous pouvez également obtenir des composants rares"),
        ("Réfracteur", [1, 5], {"PER": 6, "CHR": 7},
         "Votre résistance aux dégâts énergétiques, quelles que soit la localisation, augmente de +1 par rang dans cette aptitude"),
        ("Reins d'acier", [1, 3, 5], {"FOR": 5},
         "Votre charge maximale augmente de 12.5 kilogrammes par rang dans cette aptitude"),
        ("Résistance chimique", [1, 5], {"END": 7},
         "Au rang 1, jetez 1 D6 de moins lorsque vous déterminez si vous devenez dépendant à une drogue.\n\nAu rang 2, vous ne pouvez plus être dépendant aux drogues"),
        ("Ricochet", [5], {"END": 10},
         "Si un ennemi obtient une complication lorsqu'il vous prend pour cible d'une attaque à distance, vous pouvez dépenser 1 point de chance pour que son ricochet le touche. Résolvez les dégâts contre votre assaillant"),
        ("Robustesse", [1, 5], {"END": 6, "CHR": 6},
         "Votre résistance aux dégâts balistiques, quelle que soit la localisation, augmente de +1 par rang dans cette aptitude"),
        ("Sanguinaire", [1], {"CHA": 6},
         "Lorsque vous infligez un coup critique, jetez 1 D6 : si vous obtenez un Effet, vous infligez une blessure supplémentaire dont la localisation est tirée au hasard"),
        ("Scientifique", [2, 6, 10, 14], {"INT": 6},
         "Vous pouvez modifier les armes à énergie grâce à des mods d'arme et vous pouvez aussi fabriquer certains mods d'armure avancés. Chaque rang dans cette aptitude débloque un rang de mod : le rang 1 débloque les mods de rang 1, le rang 2 débloque les mods de rang 2, etc.."),
        ("Sens affûtés", [1], {"PER": 7},
         "Lorsque vous entreprenez l'action mineure Viser contre une cible située à portée courte ou inférieure, vous remarquez ses faiblesses et votre attaque est plus efficace. La prochaine attaque que vous exécutez contre cette cible obtient l'effet de dégâts Perforant 1. Si elle bénéficie déjà de cet effet, la valeur de ce dernier augmente de 1"),
        ("Sniper", [1], {"PER": 8, "AGI": 6},
         "Lorsque vous entreprenez l'action mineure Viser et effectuez une attaque à distance avec votre arme à deux mains Précise, vous pouvez choisir la localisation des dégâts sans augmenter la difficulté de l'attaque"),
        ("Source d'inspiration", [1], {"CHR": 8},
         "Vous menez par l'exemple, en conséquence la réserve du groupe peut contenir 1 PA supplémentaire grâce à vous"),
        ("Spécialité bonus", [5], {},
         "Vous pouvez choisir un atout personnel supplémentaire. Augmentez de 2 rangs la compétence sélectionnée, jusqu'à un maximum de 6 rangs"),
        ("Squelette adamantin", [1, 4, 7], {"END": 7},
         "Le montant de dégâts nécessaires pour vous infliger un coup critique augmente d'une valeur égale à vos rangs dans cette aptitude"),
        ("Studieux", [3, 6, 9, 12, 15, 18, 21, 24, 27, 30], {},
         "Augmentez de 2 rangs une de vos compétences ou 1 rang deux de vos compétences"),
        ("Tire-au-flanc", [4, 10], {"AGI": 6},
         "Au rang 1, lorsque vous entreprenez l'action capitale Se protéger, réduisez de 1 la difficulté du test de compétence.\n\nAu rang 2, améliorer votre défense ne vous coûte plus que 1 PA"),
        ("Tir groupé", [1], {"PER": 8, "AGI": 6},
         "Lorsque vous effectuez une attaque à distance et dépensez des munitions pour augmenter les dégâts, vous pouvez relancer jusqu'à 3 D6 de votre jet de dégâts"),
        ("Tonton flingueur", [1, 6, 11], {"AGI": 10},
         "Lorsque vous réussissez une attaque à distance, vous pouvez dépenser 1 PA et 1 munition pour toucher une cible supplémentaire située à portée courte ou inférieur de votre cible initiale. Ces deux cibles subissent les mêmes dégâts.\n\nAu rang 2, vous pouvez dépenser 2 PA et 2 munitions pour toucher deux cibles supplémentaires.\n\nAu rang 3, vous pouvez dépenser 3 PA et 3 munitions pour toucher trois cibles supplémentaires"),
        ("Tueur", [1], {"FOR": 8},
         "Lorsque vous infligez au moins 1 point dégâts à un ennemis avec une attaque à mains nues ou une arme de corps à corps, vous pouvez dépenser 1 point de chance pour lui faire subir automatiquement un coup critique sur la localisation touchée et ainsi causer une blessure"),
        ("Vendeur de junktown", [1], {"CHR": 8},
         "Réduisez de 1 la difficulté de tout test de CHR + Troc destiné à vendre ou acheter des biens"),
        ("Veuve noire/Gigolo", [1], {"CHR": 6},
         "Vous pouvez relancer 1D20 lors de tout test de compétence basé sur le CHR et destiné à influencer un personnage du genre opposé. De plus, vos attaques infligent +1 D6 de dégâts aux personnages du genre opposé"),
        ("Visée stable", [1], {"FOR": 8, "AGI": 7},
         "Lorsque vous entreprenez l'action mineure Viser, vous pouvez, au choix, relancer 2 D20 lors de votre première attaque de ce tour ou relancer 1 D20 lors de toutes vos attaques de ce tour"),
        ("Vitalité", [5, 10, 15, 20, 25], {}, "Ajoutez votre valeur d'Endurance à votre maximum de points de vie")
    )
    for aptitude in list_aptitude:
        Aptitude.objects.create(name=aptitude[0], nb_rangs=len(aptitude[1]),
                                prerequis={"LVL": aptitude[1], "SPECIAL": aptitude[2]}, description=aptitude[3])


class Command(BaseCommand):
    help = "Populate the database with all the models."

    def add_arguments(self, parser: CommandParser) -> None:
        return super().add_arguments(parser)

    def handle(self, *args: Any, **options: Any) -> None:
        populate()
