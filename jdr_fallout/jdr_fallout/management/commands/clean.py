from django.core.management.base import BaseCommand, CommandParser
from fallout.models import EffetArme, Qualites, ModelMunition, ModelArme, ModelMods, AptitudeModel
from typing import Any


def clean():
    EffetArme.objects.all().delete()
    Qualites.objects.all().delete()
    ModelMunition.objects.all().delete()
    ModelArme.objects.all().delete()
    ModelMods.objects.all().delete()
    AptitudeModel.objects.all().delete()


class Command(BaseCommand):
    help = "Remove all objects of all the models from the database."

    def add_arguments(self, parser: CommandParser) -> None:
        return super().add_arguments(parser)

    def handle(self, *args: Any, **options: Any) -> str | None:
        return clean()
